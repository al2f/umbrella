shader_type canvas_item;

uniform vec4 outlineColour: hint_color;
uniform float lineThickness: hint_range(0,10) = 1;

void fragment(){
	
	float width = lineThickness / float(textureSize(TEXTURE, 0).x);
	float height = lineThickness / float(textureSize(TEXTURE, 0).x);
	vec4 spriteColour = texture(TEXTURE, UV);
	float alpha = -4.0 * spriteColour.a;
	
	alpha += texture(TEXTURE, UV + vec2(width, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(-width, 0)).a;
	alpha += texture(TEXTURE, UV + vec2(0, height)).a;
	alpha += texture(TEXTURE, UV + vec2(0, -height)).a;
	
	vec4 finalColour = mix(spriteColour, outlineColour, clamp(alpha, 0.0, 1.0));
	COLOR = vec4(finalColour.rgb, clamp(abs(alpha) + spriteColour.a, 0.0, 1.0));
}