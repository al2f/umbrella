shader_type canvas_item;

uniform bool monochrome = false;

void fragment() {
    COLOR = texture(TEXTURE, UV);
	if (monochrome){
		float avg = (COLOR.r + COLOR.g + COLOR.b) / 3.0;
    	COLOR.rgb = vec3(avg);
	}
    
}