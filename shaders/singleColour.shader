shader_type canvas_item;
uniform vec4 colour : hint_color;

void fragment(){
	COLOR = texture(TEXTURE, UV); //read from texture
	COLOR = vec4(colour.rgb, COLOR.a);
}