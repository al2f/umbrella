shader_type canvas_item;

uniform vec2 direction = vec2(1,0.2);
uniform float speed = 5.0;
uniform vec4 fogColor: hint_color = vec4(0,0,0,1);

vec2 random( vec2 pos )
{ 
	return fract(
		sin(
			vec2(
				dot(pos, vec2(12.9898,78.233))
			,	dot(pos, vec2(-148.998,-65.233))
			)
		) * 43758.5453
	);
}

void fragment()
{
	vec2 motion = direction*speed*0.1;
	vec2 offset = TIME*motion*vec2(-1);
	COLOR = texture(TEXTURE,UV+offset);
	COLOR.a = round(COLOR.r+COLOR.g+COLOR.b)/3.0;
	COLOR.rgb = fogColor.rgb;
}