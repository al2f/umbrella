# umbrella
A game I work on in my spare time.
Apologies in advance for the messy commits early in the history of the project.

## Screenshots
![Character in a cave looking up at the waterfall he fell next to](art/screenshots/screenshot0.png "Waterfall")
![Character next to a save point](art/screenshots/screenshot1.png "Save Point")
![Character next to another waterfall climbing a vine](art/screenshots/screenshot2.png "Climbing")
![Character infront of a cave entrace](art/screenshots/screenshot3.png "Cave")
![Character gliding using umbrella](art/screenshots/screenshot4.png "Gliding")

## [Past Screenshots](art/history/history.md)

## Running
To run and/or export the game, the Godot Engine ([godotengine.org](https://www.godotengine.org)) is required. The current version used while working on the game is 3.5 stable. The `package.py` script has been written to help package the game. (Godot's export presets still need to be downloaded though.)

## Contributing
To edit the code and scenes, please use Godot v3.5 stable. (*Well, for editing the code or scenes you could use any editor you want to. Just make sure it runs as expected afterwards* 😉)
To edit the game's levels, the Tiled Map Editor ([mapeditor.org](https://www.mapeditor.org)) is required. The latest version used for editing levels was 1.9.2.

## [License](LICENSE.md)

## Discussion
There is a matrix space available for discussion at [#umbrella-game:matrix.org](https://matrix.to/#/#umbrella-game:matrix.org)
