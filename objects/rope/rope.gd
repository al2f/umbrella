extends Area2D

signal entered_by_player

onready var path := get_node("Path2D")
onready var pathFollow := path.get_node("PathFollow2D")

onready var lineCollision := get_node("lineCollision")
onready var curveDetectionArea := get_node("curveDetectionArea")

export var rope_points = []
var sag_collision = []

var line_triggered := false

var rope_start: Vector2
var rope_end: Vector2
var rope_vector: Vector2

var roping := false
var ropee

var inverse := false

func _setup(data: Dictionary):
	pass


func get_points_from_polygon():
	var ropeStaticBody = get_node("tiledObject")
	ropeStaticBody.queue_free()
	var polygonColl = ropeStaticBody.get_child(0)
	var polygon = polygonColl.polygon
	return Array(polygon)

func populate_path(points: Array):
	path.curve.clear_points()
	for p in points:
		path.curve.add_point(p)

func populate_collision(points: Array):
	for node in sag_collision:
		node.queue_free()
	
	for i in range(1, len(rope_points)):
		var c := CollisionShape2D.new()
		var l := SegmentShape2D.new()
		curveDetectionArea.add_child(c)
		sag_collision.append(c)
		
		c.shape = l
		l.a = rope_points[i-1]
		l.b = rope_points[i]

# Called when the node enters the scene tree for the first time.
func _ready():
	var points = get_points_from_polygon()
	rope_start = points[0]
	rope_end = points[1]
	if rope_start.y > rope_end.y:
		inverse = true
	rope_vector = rope_end - rope_start
	update_line_collision()
	
	rope_points = get_sag_points(20)
	populate_path(rope_points)
	populate_collision(rope_points)
	
	connect("area_entered", self, "on_rope_line_entered")
	curveDetectionArea.connect("area_entered", self, "on_rope_curve_entered")
func update_line_collision():
	lineCollision.shape.a = rope_start
	lineCollision.shape.b = rope_end

func on_rope_line_entered(area):
	if !roping:
		if area.name == "ropeHook":
			var player = area.get_parent()
			if player.velocity.y > 0:
				line_triggered = true
				ropee = area

func on_rope_curve_entered(area):
	if !roping:
		if area.name == "ropeHook":
			if line_triggered:
				var player = area.get_parent()
				if player.velocity.y > 0:
					var player_position: Vector2 = player.global_position

					player.get_parent().remove_child(player)
					pathFollow.call_deferred("add_child", player)

					var closest_offset = path.curve.get_closest_offset(path.to_local(player_position))
					pathFollow.offset = closest_offset

					player.position = Vector2.DOWN*10
	#				player.global_position = $Path2D/PathFollow2D.curve.get_closest_offset(player.global_position)
					emit_signal("entered_by_player", self)
					roping = true
				else:
					line_triggered = false

func _draw():
	if line_triggered:
		draw_line(rope_start, to_local(ropee.global_position), Color.peachpuff)
		draw_line(rope_end, to_local(ropee.global_position), Color.peachpuff)
	else:
		draw_line(rope_start, rope_end, Color.peachpuff)
	
#	var steps = 20
#	var frac = 1.0/steps
#
#	draw_circle(Vector2(0, 0), 1, Color.blue)
#
#	for i in range(0,steps+1):
#		var norm = float(i)/steps
#		var rope_point: Vector2 = rope_start + rope_vector*norm
#
#		draw_circle(rope_point, 2, Color.blue)
#		draw_circle(Vector2(norm*-rope_vector.x, get_slack(norm)), 1, Color.red)
#		draw_circle(rope_point + Vector2(0, get_slack(norm)*25), 1, Color.orange)
#
#	draw_circle(rope_start, 3, Color.green)
#	draw_circle(rope_end, 3, Color.yellow)

func get_sag_points(detail: int):
	var points = []
	
	var frac = 1.0/detail
	
	for i in range(0,detail+1):
		var norm = float(i)/detail
		var rope_point: Vector2 = rope_start + rope_vector*norm
		points.append(rope_point + Vector2(0, get_slack(norm)*25))
	return points

func _process(delta):
	update()

func get_slack(unit_offset):
	# -4\left(x-0.5\right)^{2}\ +1
	return -4 * pow(unit_offset - 0.5, 2) + 1 
