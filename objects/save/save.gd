extends Area2D

# a phrase we can "yell out" to other code to trigger a function
signal save_requested

# cooldown time
var cooldownTime = 1

# getting access to all the items in the tree
var lampColor: Color
var lampType: int
onready var cooldownTimer = get_node("Timer")
onready var light = get_node("Light2D")
onready var lightTween = light.get_node("Tween")
onready var sprite := get_node("Sprite2D")

# we want the lamp to be interactable to begin with
var interactable = true

func setup(dict: Dictionary):
	
	var default_color = Color()
	default_color.r8 = 255
	default_color.g8 = 178
	default_color.b8 = 24
	
	lampColor = dict.get("lampColor", default_color)
	
	lampType = dict.get("lampType")

# connect on_interact function
func _ready():
	
	yield(LevelManager, "levelLoaded")
	
	sprite.frame = lampType
	
	# when the cooldown timer finishes, we want to call the coolDownTimerEnd( function
	cooldownTimer.connect("timeout", self, "cooldownTimerEnd")
	# set the color of the lamp
	light.color = lampColor
	
	# we want to know when the player has entered/left the area for fading their lamp
	#warning-ignore:return_value_discarded
	get_node("lampFade").connect("body_entered", self, "fadePlayerLight")
	#warning-ignore:return_value_discarded
	get_node("lampFade").connect("body_exited", self, "brightPlayerLight")
	
	# lastly, we want to know when the player has interacted with us
	# (the player jumps collision layers when they interact)
	
	#warning-ignore:return_value_discarded
	connect("body_entered", self, "on_interact")

func brightPlayerLight(body):
	body.get_node("light").brighten()

func fadePlayerLight(body):
	body.get_node("light").dimmen()
	
# triggered on player interaction
func on_interact(body):
	# if the animation is not active, and we can be interacted with
	if not lightTween.is_active() and interactable:
		
		# yell out to the level that they need to save the game
		emit_signal("save_requested", self.name, lampType)
		# disable our interaction
		interactable = false
		
		# an unfortunately large amount of maths for the light
		# that's because the light on the lamp is flickering
		var lightAdd = light.add
		var lightStep = light.step
		
		# increase light intensity
		lightTween.interpolate_property(light, "add", light.add, lightAdd * 1.3, 0.2)
		lightTween.interpolate_property(light, "step", light.step, lightStep / 2, 0.2)
		# start the animation
		lightTween.start()
		
		# wait for 0.5 seconds
		yield(get_tree().create_timer(0.5), "timeout")
		
		# decrease light intensity
		lightTween.interpolate_property(light, "add", light.add, lightAdd, 0.5)
		lightTween.interpolate_property(light, "step", light.step, lightStep, 0.5)
		lightTween.start()
		
		# start the cooldown timer
		cooldownTimer.start(cooldownTime)

func cooldownTimerEnd():
	interactable = true
