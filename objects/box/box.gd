extends RigidBody2D

var particles = load("res://objects/0particles/boxParticles.tscn")

func destroy():
	print("Destroy")
	var part = particles.instance()
	LevelManager.level.call_deferred("add_child", part)
	part.position = self.position
	
	queue_free()
	
	

#func _on_box_body_entered(body):
#	if body.has_meta("type"):
#		if body.get_meta("type") in ["stalactite", "stalagmite"]:
#			queue_free()
