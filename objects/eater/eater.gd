extends Area2D

# connect on_interact function and add ourselves the the 'doors' group
func _ready():
	var err = connect("body_entered", self, "on_interact")
	if err != OK:
		push_error(str(err))
		get_tree().quit(1)

# triggered on player interaction, sends signal to move player to target level/door
func on_interact(body):
	if body == LevelManager.level.player:
		if body.inventory.count_item("fungus") > 0:
			body.inventory.remove_item("fungus", 1)
