extends Area2D

func setup(_dict: Dictionary):
	pass

func _ready():
	yield(LevelManager, "levelLoaded")

	#warning-ignore:return_value_discarded
	connect("body_entered", self, "on_body_entered")
	#warning-ignore:return_value_discarded
	connect("body_exited", self, "on_body_exited")

func on_body_entered(body: PhysicsBody2D):
	if body != null:
		if body.has_method("on_entered_ladder"):
			body.call("on_entered_ladder")

func on_body_exited(body: PhysicsBody2D):
	if body != null:
		if body.has_method("on_exited_ladder"):
			body.call("on_exited_ladder")
