extends Light2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var noise = OpenSimplexNoise.new()
var value := 0.0
const MAX_VALUE = 100000000
onready var initial_scale = texture_scale
onready var initial_energy = energy

export(float) var step = 10.0
export(float) var add = 1.0
export(float) var mult = 1.0
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	value = randi()
	noise.period = 16

func _process(delta):
	value += 0.5
	if value > MAX_VALUE:
		value = 0
	
	var nos = (noise.get_noise_1d(value)/step +add) * mult
	energy = nos
	texture_scale = initial_scale * energy
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# from -0.3 to 0.3
