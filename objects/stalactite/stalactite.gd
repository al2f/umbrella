extends RigidBody2D

onready var platform := get_node("platform")

onready var raycast := get_node("RayCast2D")

onready var damageArea := get_node("damageArea")

onready var timer := get_node("fallTimer")

enum States {
	HANGING,
	FALLING,
	FALLEN
}
var state = States.HANGING setget set_state

func set_state(value):
	state = value
	match state:
		States.HANGING:
			damageArea.set_deferred("monitoring", false)
			set_mode(RigidBody2D.MODE_STATIC)
		States.FALLING:
			damageArea.set_deferred("monitoring", true)
			set_mode(RigidBody2D.MODE_CHARACTER)
		States.FALLEN:
			damageArea.set_deferred("monitoring", false)
			call_deferred("set_mode", RigidBody2D.MODE_STATIC)
			position = position.snapped(Vector2.ONE)

func setup(data: Dictionary):
	pass

func _ready():
#	#warning-ignore:return_value_discarded
#	connect("body_entered", self, "on_body_entered")
	#warning-ignore:return_value_discarded
	damageArea.connect("body_entered", self, "on_damagearea_entered")
	
	timer.connect("timeout", self, "on_fall_timer_timeout")

func on_fall_timer_timeout():
	self.state = States.FALLING

func on_damagearea_entered(body):
	match state:
		States.FALLING:
			if body != null:
				if body == LevelManager.level.player:
					body.takeDamage(self.position, 1)
			
			if body.is_class("TileMap") or body.get_meta("type") == "stalactite":
				self.state = States.FALLEN
				play_sound_on_land()
			if body.get_meta("type") == "box":
				body.destroy()
func _process(delta):
	if state == States.HANGING:
		if raycast.is_colliding():
			if raycast.get_collider() == LevelManager.level.player:
				if !timer.time_left > 0:
					play_sound_on_detach()
				timer.start(0.4)

func play_sound_on_detach():
	var sound_choices = [
		"sfx_breaking_and_falling/bfh1_falling_01.ogg",
		"sfx_breaking_and_falling/bfh1_falling_06.ogg",
		"sfx_breaking_and_falling/bfh1_rock_breaking_02.ogg",
		"sfx_breaking_and_falling/bfh1_rock_breaking_03.ogg",
		"sfx_breaking_and_falling/bfh1_rock_falling_03.ogg",
		"sfx_breaking_and_falling/bfh1_rock_falling_04.ogg",
		"sfx_breaking_and_falling/bfh1_rock_falling_05.ogg"
	]
	sound_choices.shuffle()
	
	var snd = sound_choices[0]
	SoundManager.play_sound_at_pos(snd, "sfx", global_position, {"volume": 0.4})

func play_sound_on_land():
	var sound_choices = [
		"sfx_breaking_and_falling/bfh1_glass_breaking_01.ogg",
		"sfx_breaking_and_falling/bfh1_glass_breaking_05.ogg",
		"sfx_breaking_and_falling/bfh1_glass_breaking_06.ogg",
		"sfx_breaking_and_falling/bfh1_rock_breaking_01.ogg",
		"sfx_breaking_and_falling/bfh1_rock_falling_05.ogg",
		"sfx_breaking_and_falling/bfh1_rock_falling_06.ogg",
		"sfx_breaking_and_falling/bfh1_rock_falling_07.ogg",
		"sfx_breaking_and_falling/bfh1_rock_falling_08.ogg",
		"sfx_breaking_and_falling/bfh1_rock_falling_09.ogg",
	]
	sound_choices.shuffle()
	
	var snd = sound_choices[0]
	SoundManager.play_sound_at_pos(snd, "sfx", global_position, {"volume": 0.4})
