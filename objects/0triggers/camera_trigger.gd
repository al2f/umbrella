extends Area2D

func setup(_dict):
	pass

func _ready():
	yield(LevelManager, "levelLoaded")
	
	#warning-ignore:return_value_discarded
	connect("body_entered", self, "on_body_entered")
	#warning-ignore:return_value_discarded
	connect("body_exited", self, "on_body_exited")
	
func on_body_entered(body):
	if body == LevelManager.level.player:
		var rect := Rect2()
		rect.position = position - get_node("CollisionShape2D").shape.extents
		rect.size = get_node("CollisionShape2D").shape.extents*2
		body.setNewCameraBounds(rect)

func on_body_exited(body):
	if body == LevelManager.level.player:
		body.restoreCameraBounds()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
