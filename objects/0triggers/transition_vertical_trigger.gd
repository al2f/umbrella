extends "res://objects/0triggers/transition_trigger.gd"

var leftTop: bool
var rightTop: bool
var leftBottom: bool
var rightBottom: bool

func setup(dict: Dictionary):
	.setup(dict)
	
	leftTop = dict.get("leftTop", false)
	rightTop = dict.get("rightTop", false)
	leftBottom = dict.get("leftBottom", false)
	rightBottom = dict.get("rightBottom", false)

func ready():
	.ready()
