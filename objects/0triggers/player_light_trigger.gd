extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var vertical: bool
var player: KinematicBody2D
onready var collisionShape = get_node("CollisionShape2D")

var width: float
var height: float
var checkDir: float

# Called when the node enters the scene tree for the first time.
func _ready():
	yield(LevelManager, "levelLoaded")
	
	checkDir = height if vertical else width
	connect("body_entered", self, "on_body_entered")
	connect("body_exited", self, "on_body_exited")
	set_process(false)

func _process(delta):
	var distance: float
	if vertical:
		distance = abs((position - player.position).y)
	else:
		distance = abs((position - player.position).x)
		
	var multiplier = distance/ (checkDir/2)
	multiplier = clamp(multiplier, 0, 1)
	player.lamp.energy = player.lamp.initial_energy * multiplier
	
func on_body_entered(body):
	if body == LevelManager.level.player:
		player = body
		set_process(true)

func on_body_exited(body):
	if body == LevelManager.level.player:
		player = body
		set_process(false)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
