extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	yield(LevelManager, "levelLoaded")
	
	#warning-ignore:return_value_discarded
	connect("body_entered", self, "on_body_entered")
#	connect("body_exited", self, "on_body_exited")
	
func on_body_entered(body):
	if body == LevelManager.level.player:
		body.start_invincible()
		yield(get_tree().create_timer(1), "timeout")
		body.position = body.last_floor_pos

#func on_body_exited(body):
#	pass
