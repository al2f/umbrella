extends Area2D

signal previewSave

var slot := -1
var door : Area2D

func _ready():
	#warning-ignore:return_value_discarded
	connect("body_entered", self, "on_body_entered")

func on_body_entered(body: PhysicsBody2D):
	if body != null:
		emit_signal("previewSave", door, slot)
