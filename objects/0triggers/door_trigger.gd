extends Area2D

signal entered_by_player

var dest_level: String
var dest_door: String
var exit_only: bool
var width: float
var height: float
var size: Vector2

func setup(dict: Dictionary):
	dest_door = dict["dest_door"]
	dest_level = dict["dest_level"]
	
	width = dict.get("width")
	height = dict.get("height")
	size = Vector2(width, height)
	exit_only = dict.get("exit_only", false)
	

func _ready():
	yield(LevelManager, "levelLoaded")
	if exit_only:
		get_node("CollisionShape2D").disabled = true
	else:
		#warning-ignore: return_value_discarded
		connect("body_entered", self, "on_body_entered")

func on_body_entered(body: PhysicsBody2D):
	if body == LevelManager.level.player:
		emit_signal("entered_by_player", self, dest_level, dest_door, Vector2(0.5, 0.5), false)
