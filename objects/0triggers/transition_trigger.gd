extends "res://objects/0triggers/door_trigger.gd"

func ready():
	.ready()

func setup(dict: Dictionary):
	.setup(dict)

func on_body_entered(body: PhysicsBody2D):
	if body == LevelManager.level.player:
		var topLeft := global_position - size/2
		var relativePlayerPos := body.global_position - topLeft
		var proportionatePlayerPos := relativePlayerPos/size
		emit_signal("entered_by_player", self, dest_level, dest_door, proportionatePlayerPos)

func get_proportionate_position(proprotions: Vector2):
	var topLeft := global_position - size/2
	return topLeft + proprotions*size
