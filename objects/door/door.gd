extends "res://objects/0triggers/door_trigger.gd"

const doorTexturePaths = [
		"res://objects/door/frames/titlescreenDoor.png",
		"res://objects/door/frames/woodenDoor.png",
		"res://objects/door/frames/caveDoor.png",
		"res://objects/door/frames/swampDoor.png",
		"res://objects/door/frames/grassDoor.png"
]
onready var collisionShape := get_node("CollisionShape2D")
var sprite: Sprite

func setup(dict: Dictionary):
	width = dict["width"]
	height = dict["height"]

func _ready() -> void:
	yield(LevelManager, "levelLoaded")
	
	sprite = get_node_or_null("tiledObject")
	if sprite != null:
		collisionShape.shape.extents = Vector2(width/4, height/2)

func set_type(idx: int):
	if idx > len(doorTexturePaths):
		push_warning("No door texture of index %s exists!")
		return
	
	sprite.texture = load(doorTexturePaths[idx])
