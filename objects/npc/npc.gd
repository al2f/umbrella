extends Area2D



var dialogue := ""

var _activated := false setget set_activated, get_activated
func set_activated(value):
	_activated = value
	if _activated:
		LevelManager.canvasLayer.show_label(dialogue)
	else:
		LevelManager.canvasLayer.hide_label(dialogue)

func get_activated():
	return _activated

var sprite: Sprite
onready var collShape := get_node("CollisionShape2D")

func setup(dict: Dictionary):
	
	dialogue = dict.get("dialogue", dialogue)

func activate():
	self._activated = true
	
func deactivate():
	self._activated = false
	
func _ready():
	yield(LevelManager, "levelLoaded")
	
	#warning-ignore:return_value_discarded
	connect("body_entered", self, "on_entered")
	connect("body_exited", self, "on_exited")

func on_entered(body) -> void:
	if body == LevelManager.level.player:
		activate()

func on_exited(body):
	if body == LevelManager.level.player:
		deactivate()
