extends Area2D


var itemName: String
var itemId: int
var itemAmount: int
var respawnCount: int
var respawnTime: int
var infinite: bool

onready var sprite := get_node("Sprite")
onready var collShape := get_node("CollisionShape2D")

func setup(dict: Dictionary):
	add_to_group("saveOnExit")
	
	itemName = dict["itemName"]
	itemAmount = dict.get("itemAmount", 1)
	
	respawnTime = dict.get("respawnTime", 60)
	respawnCount = dict.get("respawnCount", 0)
	
	infinite = dict.get("infinite", false)
	

func _ready():
	yield(LevelManager, "levelLoaded")
	$AnimationPlayer.play("float")
	
	itemId = Inventory.new().expandItemName(itemName).id
	
	if respawnCount < 0:
		queue_free()
	else:
		sprite.texture = load("res://items/%03d.png" % itemId)
		collShape.shape.extents = sprite.texture.get_size()/2
		#warning-ignore:return_value_discarded
		connect("body_entered", self, "on_body_entered")

func on_body_entered(body: CollisionObject2D):
	if body == LevelManager.level.player:
		
		if body.pickup_item(itemName, itemAmount) == OK:
			if not infinite:
				respawnCount -= 1
				Config.storeStats(self)
				queue_free()

func get_stats() -> Dictionary:
	return {
		"respawnCount": respawnCount
	}

func set_stats(dict: Dictionary):
	respawnCount = dict["respawnCount"]
