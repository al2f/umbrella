extends Area2D

onready var sprite = get_node("AnimatedSprite")
# connect on_interact function and add ourselves the the 'doors' group
func _ready():
	connect("body_entered", self, "on_interact")
	sprite.connect("animation_finished", self, "on_sprite_animation_finished")
	add_to_group("chests")

# triggered on player interaction
func on_interact(body):
	if body == LevelManager.level.player:
		sprite.play("open")

func on_sprite_animation_finished():
	var animation_name = sprite.animation
	if animation_name == "opening":
		pass
