extends Light2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var tween = get_node("Tween")
onready var maxBrightness = energy
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func brighten():
	if not tween.is_active():
		tween.interpolate_property(self, "energy", energy, maxBrightness, 0.5)
		tween.start()

func dimmen():
	if not tween.is_active():
		tween.interpolate_property(self, "energy", energy, 0, 0.5)
		tween.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
