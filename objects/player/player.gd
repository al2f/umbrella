extends "res://prototypes/Character.gd"

## Debug 
var noclip = false
var noclip_time_held = 1

var ignore_input setget set_ignore_input, get_ignore_input

func set_ignore_input(value):
	ignore_input = value
	_animate_walk_based_on_pressed_keys("walk", "stand")

# physics related variables
var prev_velocity := Vector2(0,0)
var velocity := Vector2(0,0)
var direction := 1 setget set_direction
var invincible := false

var last_floor_pos := Vector2(0,0)
var fall_time := 0.0

var prevCameraBounds = Rect2(0,0,320, 240)

const normalGravity := 600
const fallGravity := 650

const termVel := 240
const jumpHeight := 40
# https://libreddit.net/r/godot/comments/sioxoc/ive_seen_a_lot_of_people_doing_jumps_with/
const jumpVel := sqrt(2 * normalGravity * jumpHeight)


const walkSpeed = 100
const noclipSpeed = 100

var is_on_ladder := false
var my_rope

onready var camera = get_node("Camera2D")

var umbrellaCurrentInfo := {}

# states that player can be in
enum states {
	ground,
	air,
	glide,
	zipline,
	water,
	wall,
	damage,
	climb
}

var state = states.air setget set_state
var state_changes = []

signal hearts_changed
signal no_health
var health := 3 setget set_health
var max_health := health

onready var umbrella = get_node("umbrella")
onready var sprite = get_node("playerSprite")

onready var animationTree := get_node("AnimationTree")
onready var animationPlayer := get_node("AnimationPlayer")
onready var animationState: AnimationNodeStateMachinePlayback = animationTree.get("parameters/playback")

onready var hitbox = get_node("hitbox")

onready var invincibilityTimer = get_node("invincibilityTimer")
onready var coyoteTimer := get_node("coyoteTimer")
onready var rememberJumpTimer := get_node("rememberJumpTimer")

enum itemTypes {
	mushroom,
	fabric,
	umbrella
}

const WIDTH = 10
const HEIGHT = 20

var inventory := Inventory.new()

func _ready():
	yield(LevelManager, "levelLoaded")
	
	camera.smoothing_enabled = true
	
	end_invincible()
	emit_signal("hearts_changed", health, max_health)
	
	sprite.flip_h = (direction == -1)

	invincibilityTimer.connect("timeout", self, "end_invincible")

# handle majority of player physics
func _physics_process(delta):
	### Apply general gravity
	if noclip:
		velocity.x = horizontalVel(velocity, INF, noclipSpeed*noclip_time_held/3)
		velocity.x = lerp(velocity.x, 0, noclipSpeed/10*delta)
		velocity.y = verticalVel(velocity,INF, noclipSpeed*noclip_time_held/3)
		velocity.y = lerp(velocity.y, 0, noclipSpeed/10*delta)
		
		if horizontal() or vertical():
			noclip_time_held += delta
		else:
			noclip_time_held = 1
		
	else:
		match state:
			states.ground, states.air:
				velocity.x = horizontalVel(velocity, walkSpeed, walkSpeed/5)
				velocity.x = lerp(velocity.x, 0, walkSpeed/10*delta)
				velocity.y = applyGravity(velocity, normalGravity, fallGravity, termVel, delta)
			states.glide:
				velocity.x = horizontalVel(velocity, umbrellaCurrentInfo["speeds"][0], umbrellaCurrentInfo["speeds"][0]/5)
				velocity.x = lerp(velocity.x, 0, walkSpeed/10*delta)
				velocity.y = applyGravity(velocity, umbrellaCurrentInfo["speeds"][1], umbrellaCurrentInfo["speeds"][1], umbrellaCurrentInfo["speeds"][2], delta)
			states.damage:
				velocity.y = applyGravity(velocity, normalGravity, fallGravity, termVel, delta)
				velocity.x = walkSpeed * sign(velocity.x)

	match state:
		states.ground:
			if !is_on_floor():
				animationState.travel("fall")
				self.state = states.air
		
		states.air:
			if velocity.y > 0:
				if is_on_ladder:
					if Input.is_action_pressed("ui_up"):
						self.state = states.climb
				elif umbrella.out:
					self.state = states.glide
		states.glide:
			if velocity.y < 0:
				velocity.y *= umbrellaCurrentInfo["cutVel"]
		states.zipline:
			var to_move = 150*delta
			if my_rope.inverse:
				if my_rope.pathFollow.offset - to_move < 0:
					self.state = states.air
				else:
					my_rope.pathFollow.offset -= to_move
			else:
				var rope_length = my_rope.path.curve.get_baked_length()
				if my_rope.pathFollow.offset + to_move > rope_length:
					self.state = states.air
				else:
					my_rope.pathFollow.offset += to_move
				
		
		states.water:
			pass
		
		states.climb:
			
			velocity.x = horizontalVel(velocity, walkSpeed*0.8, walkSpeed/5)
			
			velocity.y = verticalVel(velocity, walkSpeed*0.8, walkSpeed/5)
			velocity = velocity.move_toward(Vector2.ZERO, walkSpeed*10*delta)
			
			animationTree.set("parameters/climb/blend_position", velocity.normalized())
	
	prev_velocity = velocity
	
	# move by our velocity and update camera offset
	velocity = move_and_slide(velocity, Vector2.UP,
					false, 4, PI/4, false)

	
	# bring player back to floor if we are on it
	if state in [states.damage, states.air, states.glide, states.climb]:
		if is_on_floor():
			animationState.travel("stand")
			
			play_sound_on_land()
			self.state = states.ground

func play_sound_on_umbrella_open():
	if !umbrella.out:
		var num = (randi()%2)+1
		var snd = "umbrella_frame_open0%d.mp3" % num
		
		SoundManager.play_sound_at_pos(snd, "snd", global_position)

func play_sound_on_land():
	
	var volume = prev_velocity.y/termVel
	
	var leg_pos := global_position + Vector2(0, 10)
	var tm: TileMap = LevelManager.level.get_node("solidground")
	var map_pos = tm.world_to_map(tm.to_local(leg_pos))
	map_pos += Vector2.DOWN
	
	var tile_at = tm.get_cellv(map_pos)
	
	var tp = Terrain.get_tile_type(tile_at)
	
	if tp == "" or tp == null:
		return
	var snd_path = Terrain.get_land_sound_for_type(tp)
	
	SoundManager.play_sound_at_pos(snd_path, "snd", global_position, {"volume": volume})
			
# handle animation transitions
func _process(_delta):
	match state:
		states.air, states.glide:
			if animationState.get_current_node() == "jump":
				if velocity.y > 0:
					animationState.travel("fall")
		states.zipline:
			pass
		states.water:
			pass
	
# handle event triggered input
func _unhandled_input(event):
	
	if not (event is InputEventMouseMotion):
		# handle switching direction
		if state in [states.ground, states.glide, states.climb]:
			if event.is_action_pressed("ui_left"):
				self.direction = -1
				get_tree().set_input_as_handled()
			elif event.is_action_pressed("ui_right"):
				self.direction = 1
				get_tree().set_input_as_handled()
		
		if not state in [states.damage]:
			# when interacting, temporarily add player to 'interact' layer to trigger doors
			if event.is_action("ui_up"):
				get_tree().set_input_as_handled()
				set_collision_layer_bit(1, event.is_action_pressed("ui_up"))
		
			# handle umbrella opening/closing
			if inventory.count_item("umbrella") > 0:
				if not umbrella.out and event.is_action_pressed("umbrella"):
					play_sound_on_umbrella_open()
					umbrella.open()
					
					if velocity.y < 0:
						velocity.y *= umbrellaCurrentInfo["cutVel"]
			
			if umbrella.out and event.is_action_released("umbrella"):
				umbrella.close()
				if state == states.glide:
					SoundManager.play_sound_at_pos("umbrella_frame_close01.mp3", "snd", global_position)
					self.state = states.air
					
		
		match state:
			states.ground:
				_animate_walk_based_on_pressed_keys("walk", "stand")
				
				if event.is_action_pressed("jump"):
					if rememberJumpTimer.time_left <= 0:
						get_tree().set_input_as_handled()
						jump()
						self.state = states.air
				if event.is_action_pressed("ui_up"):
					if is_on_ladder:
						get_tree().set_input_as_handled()
						self.state = states.climb
			states.air:
				## Forgiving jumps. ##
				# We want to allow the player to jump off a
				# platform if they're a bit late and press jump once they've 
				# already walked off a platform.
				# 
				# We also want to allow the player to jump if they press jump early
				# when they're about to land on a platform, but are in the air currently
				
				if event.is_action_pressed("jump"):
					get_tree().set_input_as_handled()
					if coyoteTimer.time_left > 0 and velocity.y > 0:
						jump()
					else:
						rememberJumpTimer.start()
				
				# make player fall faster when releasing the jump key to allow for
				# variable jump height
				elif event.is_action_released("jump"):
					get_tree().set_input_as_handled()
					if velocity.y < 0:
						velocity.y *= 0.6
				elif event.is_action_pressed("ui_up"):
					get_tree().set_input_as_handled()
					if is_on_ladder:
						self.state = states.climb
			
			states.climb:
				if event.is_action_pressed("jump"):
					get_tree().set_input_as_handled()
					self.state = states.air
					jump()
					return
				
				var strength = Vector2(horizontal(), vertical())
				if strength.length() < 0.2:
#					event.is_action_released("ui_left") and leftStrength < 0.5 or\
#				   event.is_action_released("ui_right") and rightStrength < 0.5 or\
#				   event.is_action_released("ui_up") and upStrength < 0.5 or\
#				   event.is_action_released("ui_down") and downStrength < 0.5:
					animationState.travel("hold")
				else:
					animationState.travel("climb")
			states.zipline:
				if event.is_action_pressed("jump"):
					get_tree().set_input_as_handled()
					self.state = states.air
					jump()
					return
				elif event.is_action_pressed("ui_down"):
					get_tree().set_input_as_handled()
					self.state = states.air
					return
# apply gravity based on variables, saves typing for every state
func applyGravity(currentVelocity: Vector2, gravity: int, increasedGravity: int, terminalVelocity: int, delta: float):
	var vel = currentVelocity.y
	if vel > 0:
		vel += increasedGravity * delta
	else:
		vel += gravity * delta
	if vel > terminalVelocity:
		vel = terminalVelocity
	
	return vel

# handle horizontal movement based on variables
func horizontal():
	if self.ignore_input:
		return 0
	return Input.get_axis("ui_left", "ui_right")

func horizontalVel(currVel, speed, acc):
	var vel = currVel.x
	var axis = horizontal()
	if abs(vel) < abs(speed):
		vel += axis*acc
	return vel

func vertical():
	if self.ignore_input:
		return 0
	return Input.get_axis("ui_up", "ui_down")

# handle vertical movement based on variables - for rare cases like climbing
func verticalVel(currVel, speed, acc):
	var vel = currVel.y
	var axis = vertical()
	if abs(vel) < abs(speed):
		vel += axis*acc
	
	return vel

# move camera container up/down
func lookUpDown():
	if Input.is_action_pressed("ui_up"):
		camera.offset.y = -get_viewport().size.y/5
	elif Input.is_action_pressed("ui_down"):
		camera.offset.y = get_viewport().size.y/5
	else:
		camera.offset.y = 0

func jump():
	animationState.travel("jump")
	velocity.y -= jumpVel

func takeDamage(otherPos: Vector2, num: int) -> void:
	if not invincible:
		takeDamageSourceless(num)
		
		var difference = position-otherPos
		
		velocity.y = -jumpVel*0.8
		velocity.x = walkSpeed*sign(difference.x)
		
		self.state = states.damage

func takeDamageSourceless(num: int) -> void:
	if not invincible:
		start_invincible()
		self.health -= num
		LevelManager.level.label_health_change(position, -num)

func heal(num: int) -> void:
	self.health += num
	LevelManager.level.label_health_change(position, num)

func handle_state_change(old:int, new: int):
	state_changes.append({"old": old, "new": new, "time": Global.time})
	
	if old == states.zipline:
		my_rope.roping = false
		my_rope.line_triggered = false
		my_rope = null
	#TODO: handle falling from a distance, also opportunity to handle animations here, like viking birds
	if new == states.ground:
		
		if abs(horizontal()) != 0:
			animationState.travel("walk")
		
		if Input.is_action_pressed("ui_left"):
			self.direction = -1
		elif Input.is_action_pressed("ui_right"):
			self.direction = 1
		
		if not invincible:
			last_floor_pos = position
		
		if rememberJumpTimer.time_left > 0:
			jump()
		
	elif new == states.climb:
		if velocity.y < 0:
			velocity.y = 0
		animationState.travel("climb")
	elif new == states.air and old == states.ground:
		coyoteTimer.start()
	elif new == states.zipline:
		velocity = Vector2.ZERO

func set_state(value: int):
	handle_state_change(state, value)
	state = value

func set_health(value: int):
	health = Math.clampi(value, 0, max_health)
	emit_signal("hearts_changed", health, max_health)
	if health <= 0:
		emit_signal("no_health")
		

func set_max_health(value: int) -> void:
	max_health = value
	health = Math.clampi(health, 0, max_health)
	emit_signal("hearts_changed", health, max_health)

func set_direction(value: int) -> void:
	umbrella.flip_h = (value == -1)
	sprite.flip_h = (value == -1)
	direction = value

func _animate_walk_based_on_pressed_keys(moving_anim: String = "", standing_anim: String = ""):
	var axe = horizontal()
	if axe != 0:
		if moving_anim != "":
			animationState.travel(moving_anim)
		var direction_strength = abs(axe)
		self.direction = -1 if axe < 0 else 1
	else:
		if standing_anim == "":
			animationPlayer.stop(false)
		else:
			animationState.travel(standing_anim)

func start_invincible() -> void:
	invincible = true
	invincibilityTimer.start()
	modulate = Color(1, 1, 1, 0.2)

func end_invincible() -> void:
	invincibilityTimer.stop()
	
	invincible = false
	modulate = Color(1, 1, 1, 1)
	hitbox.set_disabled(true)
	hitbox.set_disabled(false)
	
func get_stats() -> Dictionary:
	var dict = {
		"max_health": max_health,
		"health": health,
		"direction": direction,
		"position": position,
		"inventory": inventory.get_stats(),
		"umbrella": umbrella.get_stats(),
		"state": state
	}
	
	return dict

func set_stats(dict: Dictionary) -> void:
	
	max_health =	dict["max_health"]
	health =		dict["health"]
	emit_signal("hearts_changed", health, max_health)
		
	self.direction =	dict["direction"]
	self.position =		dict.get("position", position)
	inventory.set_stats(dict["inventory"])

	umbrella.visible = inventory.count_item("umbrella") > 0
	umbrella.set_stats(dict["umbrella"])
	umbrellaCurrentInfo = umbrella.get_info()
	
	self.state = dict.get("state", states.air)
	
func setNewCameraBounds(rect: Rect2) -> void:
	prevCameraBounds.position = Vector2(camera.limit_left, camera.limit_top)
	prevCameraBounds.end = Vector2(camera.limit_right, camera.limit_bottom)
	
	camera.limit_left = rect.position.x
	camera.limit_right = rect.end.x
	camera.limit_top = rect.position.y
	camera.limit_bottom = rect.end.y

func restoreCameraBounds() -> void:
	camera.limit_left = prevCameraBounds.position.x
	camera.limit_right = prevCameraBounds.end.x
	camera.limit_top = prevCameraBounds.position.y
	camera.limit_bottom = prevCameraBounds.end.y

# Called by the ladder on any object that enters it
func on_entered_ladder():
	is_on_ladder = true
	if Input.is_action_pressed("ui_up"):
		self.state = states.climb

# Called by the ladder on any object that enters it
func on_exited_ladder():
	is_on_ladder = false
	
	if state == states.climb:
		if velocity.y < 0:
			velocity.y = 0
			jump()
		else:
			animationState.travel("fall")
		self.state = states.air
		
func puppet_jump_left():
	self.direction = -1
	self.state = states.damage
	velocity = Vector2(-walkSpeed, -jumpVel)
	print_debug("Puppet jumping left")

func puppet_jump_right():
	self.direction = 1
	self.state = states.damage
	velocity = Vector2(walkSpeed, -jumpVel)
	print_debug("Puppet jumping right")

func puppet_fall_left():
	self.direction = -1
	self.state = states.damage
	velocity = Vector2(-walkSpeed, 0)
	print_debug("Puppet falling left")

func puppet_fall_right():
	self.direction = 1
	self.state = states.damage
	velocity = Vector2(walkSpeed, 0)
	print_debug("Puppet falling right")

func pickup_item(itemName: String, itemAmount: int) -> int:
	
	var err = inventory.add_item(itemName, itemAmount)
	if err != OK:
		print_debug("err != OK")
#		print(Logger.error_to_string(err))
		return err
	
	var itemData = {
		"id": inventory.expandItemName(itemName).id,
		"amount": itemAmount
	}
	LevelManager.level.label_item(global_position, itemData)
		
	match itemName:
		"umbrella":
			# set appropriate visibility of umbrella sprite
			umbrella.set_type("frame")
			umbrellaCurrentInfo = umbrella.get_info()
			if inventory.count_item("umbrella") > 0:
				umbrella.visible = true
		"fabric":
			if inventory.count_item("fabric") >= 4:
				umbrella.set_type("fabric")
				umbrellaCurrentInfo = umbrella.get_info()
	return OK

func get_ignore_input():
	return LevelManager.canvasLayer.inventoryDisplay.visible

func on_roped(rope):
	my_rope = rope
	self.state = states.zipline
	velocity = Vector2.ZERO
