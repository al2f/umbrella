extends Area2D

onready var animatedSpriteB := get_node("AnimatedSprite")
var target: Object
var target_path: String

var activated = false setget set_activated
var objects_holding = []

func setup(data):
	if data.has("target"):
		var targetp = data["target"]
		if targetp is String:
			target_path = targetp

func set_activated(value):
	activated = value
	if activated:
		animatedSpriteB.play_bounceback("press")
	else:
		animatedSpriteB.play_bounceback("unpress")

func _ready():
	yield(LevelManager, "levelLoaded")
	target = LevelManager.level.get_object(target_path)
	#warning-ignore:return_value_discarded
	connect("body_entered", self, "on_body_entered")
	#warning-ignore:return_value_discarded
	connect("body_exited", self, "on_body_exited")
	
	self.activated = false

func on_body_entered(body):
	if not(body in objects_holding):
		objects_holding.append(body)
		if not activated:
			self.activated = true
			if target != null:
				if target.has_method("on_triggered"):
					target.call("on_triggered")

func on_body_exited(body):
	if body in objects_holding:
		objects_holding.erase(body)
		if objects_holding.empty():
			self.activated = false
			if target.has_method("on_untriggered"):
				target.call("on_untriggered")
