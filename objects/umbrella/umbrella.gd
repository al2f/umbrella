extends "res://utils/animatedSpriteBounceback.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal finished_opening
signal finished_closing

var out = false
var type := "frame" setget set_type

var stats := {
	# floating speed, gravity, terminal velocity
	"frame": {
		"speeds": [	60, 60, 200 ],
		"cutVel": 0.8,
	},
	"fabric": {
		"speeds": [ 70, 20, 40 ],
		"cutVel": 0,
	}
}

var info = stats["frame"] setget , get_info
func get_info():
	return stats[type]

func _ready():
	#warning-ignore:return_value_discarded
	connect("animation_finished", self, "on_animation_finished")

func on_animation_finished():
	match animation:
		"opening":
			emit_signal("finished_opening")
		"closing":
			emit_signal("finished_closing")



func set_type(newType: String):
	type = newType
	frames = load("res://objects/umbrella/umbrella_%s.tres" % type)

# handle animation of umbrella, keeping frame and creating a 'boing' effect
func close():
	play_bounceback("closing")
	out = false

func open():
	play_bounceback("opening")
	out = true

func get_stats() -> Dictionary:
	return {
		"type": type
	}

func set_stats(dict: Dictionary) -> void:
	self.type = dict["type"]
