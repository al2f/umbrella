extends Area2D

var itemName := ""
var itemId := -1
var ticks := 0
var growthFrame := 0
var branches := 1

onready var bushSpec: Dictionary
onready var sprite := get_node("Sprite")

# Makes sure that if a bush picks 0 as its harvest amount, that choice of a 0
# stays for the remainder of the growing frame. This is to ensure that bushes
# that didn't produce any harvest cannot be spam interacted with to force a harvest

var block_for_frame = false

func setup(dict: Dictionary):
	# make sure our stats are loaded when level loads, and saved before level exits
	add_to_group("saveOnExit")
	itemName = dict.get("itemName", "invalid")
	
	ticks = dict.get("ticks", ticks)
	branches = dict.get("branches", branches)
	
	# set our position to the bottom edge of the rectangle that shows in Tiled.
	# this ensures that the sprite set in the Tiled bushes tileset does not affect
	# the positioning of previously placed bushes.
	position.y += dict["height"]/2

func _ready():
	yield(LevelManager, "levelLoaded")

	itemId = Inventory.new().expandItemName(itemName).id
	bushSpec = Config.bushData[itemId][branches]
	
	if ticks < bushSpec["ticks"][-1]:
		LevelManager.ticker.connect("timeout", self, "onTick")
	
	#warning-ignore:return_value_discarded
	connect("body_entered", self, "on_interact")
	
	# set up spritesheet by loading image and setting n vertical frames
	sprite.texture = load("res://items/%03d_bush_%d.png" % [itemId, branches])
	sprite.vframes = len(bushSpec["ticks"])

	# make bottom edge of sprite line up with our y position
	sprite.offset.y = -sprite.get_rect().size.y/2

	# If there is no data for bush, the exisiting data from the level is used.
	
	updateFrame()

func updateFrame():
	growthFrame = getGrowthFrame()
	sprite.frame = growthFrame
	block_for_frame = false

func onTick():
	ticks += 1

	if ticks in bushSpec["ticks"]:
		updateFrame()
		if ticks >= bushSpec["ticks"][-1]:
			print("Reached peak of growth, stopping")
			LevelManager.ticker.disconnect("timeout", self, "onTick")
		
func get_stats():
	return {
		"ticks": ticks,
		"block_for_frame": block_for_frame
	}

func set_stats(dict: Dictionary) -> void:
	#NOTE: we assume that set_stats will only be called when loading from save
	ticks = dict.get("ticks", 0)
	block_for_frame = dict.get("block_for_frame", false)
	
func getGrowthFrame():
	var tickData : Array = bushSpec["ticks"]
	if ticks == tickData[-1]:
		return len(tickData)-1
	
	for i in range(len(tickData)):
		var time = tickData[i]
		if time > ticks:
			return i-1
	push_error(str("Bush ", name, ": cannot find growth frame from ", ticks, " ticks and ", tickData, " as data!"))
	return null

func on_interact(body) -> void:
	if body == LevelManager.level.player:
		var options: Array = bushSpec["produce"].slice(growthFrame, growthFrame + 1)
		
		options.shuffle()
		if not block_for_frame:
			if options[0] != 0:
				var err = body.pickup_item(itemName, options[0])
				if err == OK:
					if ticks >= bushSpec["ticks"][-1]:
						LevelManager.ticker.connect("timeout", self, "onTick")
				
					var offset = randi() % bushSpec["ticks"][1]
					ticks = offset
					print("Player interacted, regrowing with start offset of ", ticks)
					updateFrame()
			else:
				block_for_frame = true
