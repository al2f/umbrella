extends KinematicBody2D

onready var tween := get_node("Tween")
onready var coll := get_node("CollisionShape2D")

var start_closed = false
var velocity = Vector2()

var base_position: Vector2
var target_position: Vector2

enum States {
	BASE,
	TARGET,
	STALLED
}
var state = States.BASE setget handle_state_change
var found_player = false

func setup(dict: Dictionary):
	start_closed = dict.get("start_closed")

func handle_state_change(new_state):
	var old_state = state
	state = new_state
#	match new_state:
#		States.BASE:
#			position = base_position
#		States.TARGET:
#			position = target_position

func _process(delta):
	if state == States.BASE:
		velocity = position.direction_to(base_position)*25
	elif state == States.TARGET:
		velocity = position.direction_to(target_position)*25
	
	found_player = false
	for i in get_slide_count():
		var coll = get_slide_collision(i)
		if coll.get_angle(Vector2.UP) == rotation:
			velocity *=-1
			break
#		print(coll.collider)
		
		
	velocity = move_and_slide(velocity, Vector2.UP,
				false, 4, PI/4, false)
	
	
func _ready():
	yield(LevelManager, "levelLoaded")
	
	if start_closed:
		target_position = position - transform.y * Vector2(0, coll.shape.extents.y*2) 
		base_position = position
		self.state = States.BASE
	else:
		base_position = position
		target_position = position + transform.y * Vector2(0, coll.shape.extents.y*2) 
		self.state = States.BASE

# Called when the node enters the scene tree for the first time.
func on_triggered():
	self.state = States.TARGET

func on_untriggered():
	self.state = States.BASE

func set_stats(dict: Dictionary):
	self.state = dict.get("state", state)
