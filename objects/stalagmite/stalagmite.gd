extends Area2D

func setup(data: Dictionary):
	pass

func _ready():
	#warning-ignore:return_value_discarded
	connect("body_entered", self, "on_body_entered")

func on_body_entered(body: PhysicsBody2D):
	if body != null:
		if body == LevelManager.level.player:
			if max(abs(body.velocity.x), abs(body.velocity.y)) == body.velocity.y and body.global_position.y < global_position.y:
				body.takeDamage(self.position, 1)
		elif body.has_meta("type"):
			if body.get_meta("type") == "box":
				if body.linear_velocity.y > 0:
					body.destroy()
