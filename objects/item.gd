extends Object

class_name Item

var eatable: bool setget , _get_eatable
var drinkable: bool setget , _get_drinkable
var helmetable: bool setget , _get_helmetable
var capeable: bool setget , _get_capeable
var gloveable: bool setget , _get_gloveable
var bootable: bool setget , _get_bootable

var corporeal := true

var id
var amount

var on_eat: Dictionary
var on_drink: Dictionary
var on_helmet: Dictionary
var on_cape: Dictionary
var on_glove: Dictionary
var on_boot: Dictionary
	
func _init(dict: Dictionary):
		
		self.on_eat =		dict.get("on_eat", {})
		self.on_drink =		dict.get("on_drink", {})
		self.on_helmet =	dict.get("on_helmet", {})
		self.on_cape =		dict.get("on_cape", {})
		self.on_glove =		dict.get("on_glove", {})
		self.on_boot =		dict.get("on_boot", {})
		self.amount =		dict.get("amount", 1)
		
		self.corporeal =	dict.get("corporeal", true)
		self.id =			dict.get("id", -1)
	
func _get_eatable():
	return not self.on_eat.empty()

func _get_drinkable():
	return not self.on_drink.empty()

func _get_helmetable():
	return not self.on_helmet.empty()

func _get_capeable():
	return not self.on_cape.empty()

func _get_gloveable():
	return not self.on_glove.empty()

func _get_bootable():
	return not self.on_boot.empty()
