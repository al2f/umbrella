class_name Inventory
extends Object

signal inventoryFull
signal itemChanged

var items := []
var maxSlots := 10 setget set_maxSlots
var stackData: Array = Config.itemData["info"]["stacks"]
var templateData: Dictionary = Config.itemData["templates"]

func _init():
	set_maxSlots(maxSlots)

func set_maxSlots(value: int):
	maxSlots = value
	
	if len(items) < maxSlots:
		for _i in range(maxSlots - len(items)):
			items.append({})
	elif len(items) > maxSlots:
		printerr("Inventory has more items than its max slots will allow!")

func count_item(itemName: String):
	var indexes = find_item_all(itemName)
	var total := 0
	
	for i in indexes:
		total += items[i].amount
	
	return total

func taken_slots() -> int:
	var invWithoutEmpty = items.duplicate(true)
	
	for a in invWithoutEmpty:
		if a.empty():
			invWithoutEmpty.erase(a)
	
	return len(invWithoutEmpty)
	
func free_slots() -> int:
	return maxSlots - taken_slots()
	
func open_slots_for_item(itemName: String):
	var freeSlots := 0
	
	var maxStack = stackData[expandItemName(itemName).id]

	var emptySlots = free_slots()
	freeSlots += emptySlots * maxStack
	
	for i in find_item_all(itemName):
		freeSlots += (maxStack - items[i].amount)
	
	return freeSlots

func add_item(itemName: String, amount := 1):
	var maxStack = stackData[expandItemName(itemName).id]
	var spaceNeeded = ceil(float(amount)/maxStack)
	
	if free_slots() < spaceNeeded or find_available() == -1:
		emit_signal("inventoryFull", itemName, amount)
		return FAILED

	var itemsToAdd := amount
	
	for _i in range(spaceNeeded):
		if itemsToAdd <= 0:
			print_debug("In loop, 0")
			return OK
		# when we can add a whole slot of items
		elif itemsToAdd >= maxStack:
			print_debug("Adding %s" % maxStack)
			add_item_to_end({"type": itemName, "amount": maxStack})
			itemsToAdd -= maxStack
		# when we add less than a whole slot of items
		elif itemsToAdd < maxStack:
			print_debug("Adding %s" % itemsToAdd)
			add_item_to_end({"type": itemName, "amount": itemsToAdd})
			itemsToAdd = 0

	print(itemsToAdd)
	if itemsToAdd <= 0:
		print_debug("Out of loop, 0")
		return OK

func add_item_to_end(itemDict: Dictionary):
	var idx = find_available()
	if idx != -1:
		items[idx] = itemDict
		emit_signal("itemChanged", idx)

func remove_item(itemName: String, amount := 1):
	
	if count_item(itemName) < amount:
		printerr("Not enough of item ", itemName, " in inventory to remove ", amount, " of it")
	else:
		var output := []
		var itemsToRemove := amount
		var indexes = find_item_all(itemName)
		indexes.invert()
		
		for i in indexes:
			if itemsToRemove <= 0:
				return output
			
			# when we can remove all the items in a slot
			elif itemsToRemove - items[i].amount >= 0:
				var count: int = items[i].amount
				itemsToRemove -= count
				output.append(items[i])
				items[i].clear()
				emit_signal("itemChanged", i)
				
			# when we only need to remove part of the items from a slot
			else:
				output.append(items[i].duplicate(true))
				output[-1].amount = itemsToRemove
				items[i].amount -= itemsToRemove
				
				emit_signal("itemChanged", i)
				
				itemsToRemove = 0

func remove_item_at_slot(slot: int, amountToTake: int):
	if items[slot].empty():
		return
	
	if amountToTake > items[slot]["amount"]:
		printerr("Cannot remove %d items from slot %d as there is only %d in the slot" % [amountToTake, slot,items[slot]["amount"]])
		return
	elif amountToTake == items[slot]["amount"]:
		items[slot]["amount"] = 0
		items[slot].clear()
	else:
		items[slot]["amount"] -= amountToTake
	
	emit_signal("itemChanged", slot)
func find_item_all(itemName: String):
	var indexes = []
	
	for i in range(0, len(items)):
		var item: Dictionary = items[i]
		if item.get("type") == itemName:
			indexes.append(i)
	return indexes

func find_item(itemName: String):

	for i in range(0, len(items)):
		var item: Dictionary = items[i]
		if item.get("type") == itemName:
			return i
	return -1

func find_available():
	# TODO: Intended slot detection for inventory would to find the last group
	# of empty slots and return its first item
	# eg. [f][ ][f][f][ ][ ][f] would be filled to
	#     [f][ ][f][f][#][ ][f] then
	#     [f][ ][f][f][f][#][f] then
	#     [f][#][f][f][f][f][f]
	var reversed := items.duplicate(true)
	reversed.invert()
	
	if free_slots() > 0:
		for i in range(len(items)):
			if reversed[i].empty():
				return len(items)-1-i
		return -1
	else:
		return -1
	
func expandItemName(itemName: String):
	return templateData[itemName]

func expandItem(item: Dictionary):
	var result = expandItemName(item.type)
	result["amount"] = item.amount
	return result

func combine_slots(first: int, second: int):
	var firstItem: Dictionary = items[first]
	var secondItem: Dictionary = items[second]
	
	if firstItem.empty() or secondItem.empty():
		printerr("Cannot combine slots ", first, " and ", second, " as one or more of the slots are empty")
	elif expandItem(firstItem).id != expandItem(secondItem).id:
		printerr("Cannot combine slots ", first, " and ", second, " as they are not of the same id")
	else:
		var amountFrom = firstItem.amount
		var amountTo = secondItem.amount
		
		var maxStack = stackData[expandItemName(firstItem.type).id]
		
		# the sum is less than the max stack amount, meaning we can fully clear first slot
		if amountFrom + amountTo <= maxStack:
			items[second].amount += amountFrom
			items[first].clear()
			emit_signal("itemChanged", first)
			emit_signal("itemChanged", second)
		else:
			# the second slot is already full, meaning we abort
			if amountTo >= maxStack:
				print_debug("Slot to is already full, returning")
				return
			else:
				var possibleMax = maxStack - amountTo
				print_debug("Possible max into second slot is ", possibleMax)
				
				# we can fully clear first slot
				if amountFrom <= possibleMax:
					
					print_debug("First has less or equal to maximum, clearing it")
					items[second].amount += amountFrom
					items[first].clear()
					emit_signal("itemChanged", first)
					emit_signal("itemChanged", second)
				# we must only take part of the first slots' items
				else:
					print_debug("First has above maximum, only taking part")
					items[first].amount -= possibleMax
					items[second].amount += possibleMax
					emit_signal("itemChanged", first)
					emit_signal("itemChanged", second)

func swap_slots(first: int, second: int) -> void:
	var firstCopy = items[first].duplicate(true)
	items[first] = items[second]
	items[second] = firstCopy
	emit_signal("itemChanged", first)
	emit_signal("itemChanged", second)

func get_stats() -> Dictionary:
	return {
		"items": items,
		"maxSlots": maxSlots
	}

func set_stats(dict: Dictionary):
	items = dict["items"]
	self.maxSlots = dict["maxSlots"]

func clear() -> void:
	for item in items:
		item.clear()
