extends "res://objects/door/door.gd"

@onready var heartsFull := get_node("heartsFull")
@onready var heartsEmpty := get_node("heartsEmpty")

func _ready() -> void:
	super._ready()

func setHearts(num: int):
	for heart in heartsFull.get_children():
		if int(String(heart.name)) < num:
			heart.show()
		else:
			heart.hide()

func setMaxHearts(num: int):
	for heart in heartsEmpty.get_children():
		if int(String(heart.name)) < num:
			heart.show()
		else:
			heart.hide()

func hideHearts():
	heartsFull.hide()
	heartsEmpty.hide()

func showHearts():
	heartsFull.show()
	heartsEmpty.show()
