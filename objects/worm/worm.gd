extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var speed := 500
var velocity := Vector2(speed, 0)
#onready var hitbox := $hitbox

func _ready():
	add_to_group("enemies")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	velocity = Vector2(speed, 0)
	velocity = move_and_slide(velocity*delta)
	
	for i in range(get_slide_count()):
		var collision: KinematicCollision2D = get_slide_collision(i)
		var collider = collision.collider
		if collider.is_in_group("player"):
			collider.takeDamage(self.position, 1)
