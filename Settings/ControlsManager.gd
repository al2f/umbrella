extends Node

## Adapted from lemilonkh/alghost's code: https://gist.github.com/lemilonkh/97cb8935ed210423748b993077bdc7a4

signal changed

const CONTROLS_SAVE_PATH := "user://controls.tres"
var ControlsData = preload("res://Settings/ControlsData.gd")

const CONTROLS_WHITELIST = [
	"ui_accept",
	"ui_select",
	"ui_cancel",
	"ui_left",
	"ui_right",
	"ui_up",
	"ui_down",
	"ui_page_up",
	"ui_page_down",
	"jump",
	"umbrella",
	"quit",
	"inventory",
	"settings",
	"debug",
	"screenshot",
	"fullscreen"
]
func _ready() -> void:
	var fileObj := File.new()
	if !fileObj.file_exists(CONTROLS_SAVE_PATH):
		save_controls()
	load_controls()

func save_controls() -> void:
	var actions := InputMap.get_actions()
	var data = ControlsData.new()
	for action in actions:
		if action.begins_with("editor_"):
			continue
		if action in CONTROLS_WHITELIST:
			
#			data.controls[action] = InputMap.action_get_events(action)
			data.controls[action] = InputMap.get_action_list(action)
	var error := ResourceSaver.save(CONTROLS_SAVE_PATH, data)
	print(data)
	if error != OK:
#		printerr("Failed to save controls! Error: ", error_string(error))
		printerr("Failed to save controls! Error: ", error)

func load_controls() -> void:
	if not ResourceLoader.exists(CONTROLS_SAVE_PATH):
		printerr("No saved controls data in ", CONTROLS_SAVE_PATH)
		return

	var data = ResourceLoader.load(CONTROLS_SAVE_PATH)
	if not is_instance_valid(data):
		printerr("Failed to load controls!")
		return

	for action_name in data.controls.keys():
		for event in data.controls[action_name]:
			InputMap.action_add_event(action_name, event)

	emit_signal("changed")

func clear_action_mapping(action_name: String, use_controller: bool) -> void:
#	for event in InputMap.action_get_events(action_name):
	for event in InputMap.get_action_list(action_name):
		InputMap.action_erase_event(action_name, event)
