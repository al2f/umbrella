#!/usr/bin/python3
import configparser
import re
import sys
import os
import subprocess
import zipfile
from pathlib import Path

PROJECT_NAME="umbrella"

def usage(presets = {}):
	print("Packages godot project based on project presets and project.godot.\n")
	print("Usage: %s [options] [commands] [preset names]\n" % sys.argv[0])
	print("""Options:
	-d | --debug          Package debug build. Defaults to false.
	-h | --help           Displays this help. Defaults to false.
	-i | --interactive    If no presets are provided, prompt for a single preset to use. Defaults to true.
	-U                    Only update build info, but do not package

Commands:
	all     Package all available presets (this may take a while)
	help    Displays this help
""")

	print("Available presets:")
	if presets != {}:		
		presetNames = [presets[k]["name"] for k in presets.keys()]
		print( "\t" + "\n\t".join(presetNames) )
	else:
		print("	No presets found. Make sure you are in a godot project folder that has project.godot and export_presets.cfg")

def get_presets():
	config = configparser.ConfigParser()
	successfully_read = config.read("export_presets.cfg")

	presets = {}

	for sectionName in config.sections():
		if re.match("^preset\.[0-9]+$", sectionName):
			section = config[sectionName]
			presetNum = int(sectionName.lstrip("preset."))
			presetName = section.get("name")
			presetPath = section.get("export_path")
			
			presetName = remove_quotes(presetName)
			presetPath = remove_quotes(presetPath)
			
			presets[presetNum] = {"name": presetName, "path": presetPath}
	return presets

def get_preset_names(dic: dict):
	output = ""
	for number in presets.keys():
		name = presets[number]["name"]
		output += "%d:\t%s\n" % (number, name)
	return output

def search_for_preset(presetName, presets: dict):
	for k in presets.keys():
		if presets[k]["name"] == presetName:
			return presets[k]

def remove_quotes(string: str):
	if string.startswith("'") and string.endswith("'"):
		return string[1:-1]
	elif string.startswith("\"") and string.endswith("\""):
		return string[1:-1]
	else:
		raise Exception("String is not surrounded by quotation marks!")

def update_build_info():
	commit = str(subprocess.check_output("git rev-parse HEAD", shell=True, stderr=subprocess.DEVNULL), 'utf-8')

	tags = str(subprocess.check_output("git describe --tags --long", shell=True, stderr=subprocess.DEVNULL), 'utf-8').split('\n')
	most_recent_tag = tags[0]
	things = most_recent_tag.split("-")
	print(things)
	if len(things) == 3:
		tag, since, commit = things
		commit = commit.lstrip("g")
		a = open("hash.txt", "w+")
		content = """[main]
	commit="{0}"
	tag="{1}"
	since={2}
	""".format(commit, tag, since)
		try:
			a.write(content)
		except (IOError, OSError):
			print("Error writing to file")
			sys.exit(1)
		a.close()
		
		print("Updated build info\n")
		print(content)
	else:
		print("Failed getting build information. Make sure git is installed")
		sys.exit(1)
	
def build(presets: dict, debug: bool):
	update_build_info()
	for preset in presets:
		args = ["godot3.5", "--no-window"]
		
		if debug:
			args.append("--export-debug")
		else:
			args.append("--export")
		
		args.append(preset["name"])
		args.append(preset["path"])
		
		try:
			print(f"Packaging %s for \"%s\"...\n" % (PROJECT_NAME, preset["name"]), end="", flush=True)
			print(args)
			subprocess.run(
				args,
				stdin=sys.stdin,
				stdout=sys.stdout,
				stderr=sys.stderr,
				check=True
			)
			binaryPath = Path(preset["path"])
			binaryName = Path(preset["path"]).name
			folderWithBinary = Path(preset["path"]).parent
			packName = binaryPath.with_suffix(".pck").name
			zipName = binaryPath.with_name(PROJECT_NAME+"_"+preset["name"] + ".zip").name
			
			filenames = [binaryPath, folderWithBinary/packName]
			print(filenames)
			with zipfile.ZipFile(folderWithBinary / zipName, mode="w") as archive:
				for filename in filenames:
					archive.write(filename, filename.name)
			print("Wrote " + str(folderWithBinary / zipName))
			
		except subprocess.CalledProcessError as e:
			print(e)
			sys.exit(1)
		

presets = get_presets()
		
args = sys.argv[1:]

interactive = True
build_all = False
package_debug = False
presets_to_package = []

while args != []:
	if args[0] in ["-h", "--help", "help"]:
		usage(presets)
		sys.exit(0)
	elif args[0] == "all":
		build_all = True
	elif args[0] in ["-d", "--debug"]:
		package_debug = True
	elif args[0] in ["-U"]:
		update_build_info()
		sys.exit(0)
	else:
		if args[0] in [presets[k]["name"] for k in presets.keys()]:
			presets_to_package.append(search_for_preset(args[0], presets))
			print(presets_to_package)
		else:
			raise Exception("Unknown argument " + args[0])
	args = args[1:]

if presets == {}:
	print("No presets found. Make sure you are in a godot project folder that has project.godot and export_presets.cfg")
	sys.exit(1)
	quit()
	exit()

if build_all:
	print("Building all!")
	presets_to_package = [presets[k] for k in presets.keys()]
else:
	if presets_to_package == []:
		print("Available presets:")
		for k in presets.keys():
			print("%d	%s" % (k, presets[k]["name"]) )

		presetNum = int(input("Enter preset number to package: "))
		presets_to_package.append(presets[presetNum])

build(presets_to_package, package_debug)	
