extends Node2D

# Name of currently loaded level.
var _currentLevel = "titlescreen"

# Currently loaded game slot. -1 is used for undetermined/empty slots
# eg. when on the titlescreen.
var currentSlot = -1

# reference to the current level node
var level

# reference to the canvasLayer. (ui/gui.gd)
onready var canvasLayer := get_node("CanvasLayer")

# Ticker timer that times out once per second.
onready var ticker = get_node("tickTimer")

# The levelLoaded signal is emitted when the level has been built and is ready for playing.
signal levelLoaded

func _ready():
	ticker.start()
#	get_viewport().connect("gui_focus_changed", self, "_on_focus_changed")
		
# Pauses game when focus is lost, and resumes game when focus is gained.
func _notification(what):
	
	# handle pausing/resuming game when focus is lost
	if what == MainLoop.NOTIFICATION_WM_FOCUS_OUT:
		pass
#		get_tree().paused = true
#		LevelManager.canvasLayer.show_settings()
		#OS.request_attention()

# Stores all stats of nodes in the `saveOnExit` group.
func storeStatsInLevel():
	for object in get_tree().get_nodes_in_group("saveOnExit"):
		Config.storeStats(object)

# Restores all stats of nodes in the `saveOnExit` group.
func restoreStatsInLevel():
	for object in get_tree().get_nodes_in_group("saveOnExit"):
		Config.restoreStats(object)

# Disconnects connections from the player and level prior to level removal.
# Stores stats of objects in level.
func handleConnectionsPre() -> void:
	level.disconnect_signals()
	
	for connection in get_incoming_connections():
		connection["source"].disconnect(connection["signal_name"], self, connection["method_name"])
	
	level.player.disconnect("hearts_changed", canvasLayer, "update_hearts")
	level.player.inventory.disconnect("inventoryFull", canvasLayer, "on_inventoryFull")
	level.player.inventory.disconnect("itemChanged", canvasLayer.inventoryDisplay, "onItemChanged")
	
	storeStatsInLevel()

# Reconnect connections to the player and level post level creation.
# Restores stats of objects in level.
func handleConnectionsPost() -> void:
	
	level.reconnect_signals()
	
	for door in level.get_objects_by_type("door"):
		door.connect("entered_by_player", self, "move_player_to_door_on_level")
	
	for area in level.get_triggers_by_type("transition"):
		area.connect("entered_by_player", self, "move_player_to_door_on_level")
	
	for area in level.get_triggers_by_type("transition_vertical"):
		area.connect("entered_by_player", self, "move_player_to_door_on_level")
	
	for save in level.get_objects_by_type("save"):
		save.connect("save_requested", self, "save_game")
	
	
	#warning-ignore:return_value_discarded
	level.player.connect("hearts_changed", canvasLayer, "update_hearts")
	#warning-ignore:return_value_discarded
	level.player.connect("no_health", self, "on_player_death")
	#warning-ignore:return_value_discarded
	level.player.inventory.connect("inventoryFull", canvasLayer, "on_inventoryFull")

	level.player.inventory.connect("itemChanged", canvasLayer.inventoryDisplay, "onItemChanged")
	
	restoreStatsInLevel()



# Save the game to current slot where `save` is the save point that triggered this.
func save_game(save_name: String, save_type: int) -> void:
	canvasLayer.messageContainer.add_message("MESSAGE_SAVED")
	
	print("Saving game, slot is ", currentSlot)
	Global.captureScreen(str("user://save", currentSlot, ".png"))
	
	var saveFile = ConfigFile.new()
	
	# get data from all saveOnExit objects, and place into saveBuffer
	storeStatsInLevel()
	Config.saveBuffer["general"]["player"] = level.player.get_stats()
	Config.saveBuffer["game"] = {
		"save_name": save_name,
		"lampType": save_type,
		"level": _currentLevel
	}
	
	# Copy over all info from saveBuffer into saveData, with overwrite
	DictUtils.merge_recursive(Config.saveData, Config.saveBuffer)
	
	for key in Config.saveBuffer:
		match typeof(Config.saveBuffer[key]):
			TYPE_ARRAY, TYPE_DICTIONARY:
				Config.saveBuffer[key].clear()
	
	for topic in Config.saveData:
		for subtopic in Config.saveData[topic]:
			saveFile.set_value(topic, subtopic, Config.saveData[topic][subtopic])
	
	saveFile.save(str("user://save", currentSlot, ".cfg"))

func new_game(slot: int, target_level = null) -> void:
	currentSlot = slot
	if target_level == null:
		loadTiledLevel("tutorial")
	else:
		loadTiledLevel(target_level)
	
	yield(self, "levelLoaded")
	
	handle_command_options_after_level_load()
	
# Load the game from a specific slot, optionally passing a level name to load
func load_game(slot: int, target_level = null) -> void:
	currentSlot = slot
	Config.saveData = Config.save_to_dict(slot)
	var saveDict = Config.saveData
	
	if saveDict.has("game"):
		if target_level == null:
			# If no level was provided, load the level based on the name from the save file
			_currentLevel = saveDict["game"]["level"]
		else:
			# If a specific level was provided, load the specific level instead, additionally
			# do not load the player's position from the save file, as the saved position
			# is not going to correspond to the specific level. Instead, we remove
			# the position from the dictionary to be loaded, placing the player in
			# the default spawn position in the specific level
			
			_currentLevel = target_level
			saveDict.erase("position")
		
		# load level, then set the reference to the player and link up any signals we need.
		loadTiledLevel(_currentLevel)

		yield(self, "levelLoaded")
		
		# restore player stats from save file
		level.player.set_stats(saveDict["general"]["player"])
		
		handle_command_options_after_level_load()
		
		#NOTE: the only place where we want to fill the inventory explicitly is when
		# we're loading a save as most, if not all of the items will be different. 
		canvasLayer.inventoryDisplay.fill()
		
	else:
		push_error("Save file %d.cfg does not have 'game' section!" % slot)
	

func handle_command_options_after_level_load():
	var p = Config.options.get("custom_position")
	if p != null:
		level.player.global_position = p
	var n = Config.options.get("spawn_at_name")
	if n != null:
		if level.get_object(n) != null:
			level.player.global_position = level.get_object(n).global_position

# Return the name of the current level
func getLevelName():
	return _currentLevel

# Remove current level.
# Player stats are **not** transferred between levels.
func dropLevel() -> void:
	handleConnectionsPre()
	
	level = get_node("/root/level")
	
	# we want the name to be different so that we do not get a 'level@2' or similar which would break
	# the whole system. Using `yield` in a function often makes it so that the rest of the code does
	# not wait for that function to finish, again causing issues.
	level.name = "some placeholder"
	level.queue_free()

# Drop level, then load new one.
# Player and object stats are transferred between levels.
func dropAndLoadLevel(levelName: String, post_player_add_function: Array = ["", []]) -> void:
	get_viewport().render_target_update_mode = Viewport.UPDATE_DISABLED
	
	# store player stats before removing level
	var prevStats = level.player.get_stats()
#	print("Previous stats ", player.get_stats())
	dropLevel()
	loadTiledLevel(levelName, post_player_add_function)
	yield(self, "levelLoaded")
	
	get_viewport().render_target_update_mode = Viewport.UPDATE_WHEN_VISIBLE
	
	# do not restore the position from the stats dict when transitioning between
	# levels
	if "position" in prevStats:
		prevStats.erase("position")
	
	# and load them back in after
	level.player.set_stats(prevStats)

# Loads and builds a level from a tiled Tiled .tmx file.
# Once created, the level is added as 'level/'
func loadTiledLevel(levelName: String, post_player_add_function: Array = ["", []]) -> void:
	print_debug("Started loading level")
	
	# set current level to be the one we are loading.
	_currentLevel = levelName
	var levelPath = "res://tiled/levels/" + levelName + ".tmx"
	
	# create instance from vnen's Tiled importer to import the map
	var tiledMapReader = load("addons/vnen.tiled_importer/tiled_map_reader.gd").new()
	var optionsDict = {
		"add_background":true,
		"collision_layer":8,
		"custom_properties":true,
		"embed_internal_images":false,
		"image_flags":0,
		"post_import_script":null,
		"save_tiled_properties":true,
		"tile_metadata":false,
		"uv_clip":true
	}
	
	# build the level
	var result = tiledMapReader.build(levelPath, optionsDict)
	
	if typeof(result) == TYPE_INT:
		Logger.print_err(result, "Error when building Tiled scene: ")
		get_tree().quit()
	
	level = result
	
	# set the level name and add as a child
	level.name = "level"
	
	var cameraBounds := Rect2(0,0,0,0)
	
	# loop through all the pseudo-objects from the level in Tiled and instance
	# actual objects in their place based on their 'type' property
	for node in level.get_children():
		# tile layers imported as TileMap
		# nodes and object layers imported as Node2D
		if node is TileMap:
			pass
			if node.name in ["solids", "climbable"]:
				node.visible = false
			
		elif node is Node2D:
			
			# loop through all children of layer
			for tiledObject in node.get_children():
				
				# read the metadata(properties) or the object and save them to a dict
				var properties := meta_to_dict(tiledObject)
				var godotNode: Object
				
				# create path for object to load
				# res://objects/type/type.tscn
				var type := ""
				if "class" in properties:
					type = properties["class"]
				elif "type" in properties:
					type = properties["type"]
				else:
					push_warning("No type found for object " + tiledObject.name + " in level file '" + _currentLevel + "'")
				
				var defaults = Config.objectTypesData
				if defaults.has(type):
					for objProp in defaults[type]:
						if not properties.has(objProp):
							properties[objProp] = defaults[type][objProp]
							print("Setting default property %s of %s to %s " % [objProp, type, defaults[type][objProp]])
				
				if node.name == "meta":
					match properties["type"]:
						"background":
							var parallaxBackground := ParallaxBackground.new()
							parallaxBackground.name = "ParallaxBackground"
							level.add_child(parallaxBackground, true)
							
							for property in properties:
								var parts: Array = property.split("/")
								
								if len(parts) == 2:
									var layerNumber: int = int(parts[0])
									
									var layerProperty: String = parts[1]
									
									if parallaxBackground.get_node_or_null("layer%d" % layerNumber) == null:
										var parallaxLayer := ParallaxLayer.new()
										parallaxBackground.add_child(parallaxLayer)
										parallaxLayer.name = "layer%d" % layerNumber
									
									var parallaxLayer : ParallaxLayer = parallaxBackground.get_node("layer%d" % layerNumber)
									
									match layerProperty:
										"scroll_x":
											parallaxLayer.motion_scale.x = properties[property]
										"scroll_y":
											parallaxLayer.motion_scale.y = properties[property]
										"texture":
											var sprite := Sprite.new()
											sprite.centered = false
											
											var texture: Texture = load("res://art/%s" % properties[property])
											sprite.texture = texture
											parallaxLayer.add_child(sprite, true)
											
											parallaxLayer.motion_mirroring = texture.get_size()
										"offset_x":
											pass
										"offset_y":
											pass
							
					if properties["name"] == "level":
						#TODO: set up music
						var c = 1- properties["darkness"]
						var canvasModulate = CanvasModulate.new()
						canvasModulate.name = "darkness"
						level.add_child(canvasModulate)
						canvasModulate.color = Color(c, c, c)
						
						cameraBounds = Rect2(tiledObject.position, Vector2(properties["width"], properties["height"]))
						
						node.remove_child(tiledObject)
						
						if not properties.get("script") in [null, ""]:
							print("setting level script to res://tiled/", properties["script"])
							level.set_script(load("res://tiled/" + properties["script"]))
						else:
							level.set_script(load("res://tiled/level_with_player.gd"))
							level.post_player_add_function = post_player_add_function
					# Remove object that was on meta layer, as we don't want a walkable "background"
					# object for instance.
					tiledObject.queue_free()
					
				elif node.name == "triggers":
					godotNode = Area2D.new()
					
					if tiledObject is StaticBody2D:
						var path = "res://objects/0triggers/" + type + "_trigger.gd"
						var triggerScript = load(path)
						
						for thing in tiledObject.get_children():
							assert(thing is CollisionShape2D)
							thing.name = "CollisionShape2D"
							thing.position -= Vector2(properties["width"], properties["height"])/2
							tiledObject.remove_child(thing)
							godotNode.add_child(thing)
						
						print("Set up ", type, " trigger")
						godotNode.set_script(triggerScript)
				else:
					if not ["label", "point"].has(type):
						if type != "":
							var path = "res://objects/" + type + "/" + type + ".tscn"
							
							# load object
							var godotScene := load(path)
							
							if (godotScene == null):
								push_error(str("Cannot load object '", tiledObject.name, "' from level ", _currentLevel, " missing type."))
							else:
								godotNode = godotScene.instance()
								godotNode.name = tiledObject.name
				
				if godotNode:
					# set appropriate visibility
					#NOTE: "visible" does not exist unless it is set in Tiled to anything other than the default
					# which is why we need this check
					if properties.has("visible"):
						godotNode.visible = properties["visible"]
						properties.erase("visible")
					
					if godotNode.is_class("Node2D"):
						godotNode.position = tiledObject.position
						
						# Tiled uses bottom left corner for tile objects
						# we need to account for that
						if tiledObject is Sprite:
							pass
							godotNode.position.y -= properties["height"]
							
						# our objects are centered, apply offset
						godotNode.position.x += properties["width"]/2
						godotNode.position.y += properties["height"]/2
						
						# apply 1px offset
						#godotNode.position.y += 1
					
					# if the property is defined in the objects' script, we set it
					# otherwise it is ignored (so we don't have properties defined
					# on the spot.
					
					# NOTE: after reading this again, I realise that `object.set(`
					# does not set the property if it does not exist in the first place.
					# a workaround for this is using set_meta and get_meta.
					
					set_properties_for_object(godotNode, properties)
					
					if tiledObject.is_class("Node2D"):
						if godotNode.is_class("Node2D"):
							godotNode.rotation = tiledObject.rotation
					
					if properties.get("keep") == true:
						node.remove_child(tiledObject)
						tiledObject.name = "tiledObject"
						godotNode.add_child(tiledObject)
						tiledObject.position = Vector2(-properties["width"],properties["height"])/2
					else:
						tiledObject.free()
					
					if godotNode.has_method("setup"):
						godotNode.setup(properties)
					else:
						push_warning(str(godotNode.name, " on level ", _currentLevel, " does not have setup method!"))
					
					node.add_child(godotNode)
					godotNode.set_owner(level)
	
		# fix up z indexes
		match node.name:
			"solidground":
				node.z_index = 0
			"foreground":
				node.z_index = 1
			"background":
				node.z_index = -1
			"background2":
				node.z_index = -2
	
	
	var scnProperties = meta_to_dict(level)
	var tileSize = Vector2(scnProperties["tilewidth"], scnProperties["tileheight"])
	
	var deathArea := Area2D.new()
	deathArea.z_index = -100
	var deathColl := CollisionShape2D.new()
	var deathShape := RectangleShape2D.new()
	
	deathArea.name = "pit"
	
	deathArea.set_script(load("res://objects/0triggers/floor_trigger.gd"))
	
	# Place the pit 3 tiles below the borrom of the level, and at half the width of the level
	deathArea.position = Vector2(scnProperties["width"]/2,scnProperties["height"]) * tileSize + Vector2(0, 3)*tileSize
	
	deathShape.extents = Vector2(scnProperties["width"], 3) * tileSize
	deathColl.shape = deathShape
	deathArea.add_child(deathColl)
	
	level.add_child(deathArea)
	
	yield(get_tree(), "idle_frame")
	level.setLevelManager(self)
	get_node("/root").add_child(level)
	
	
	yield(get_tree(), "idle_frame")
	
	var climbableTilemap = level.get_node_or_null("climbable")
	if climbableTilemap == null:
		printerr("Level %s is missing a 'climbable' tile layer!" % _currentLevel)
		get_tree().quit(1)
	var climbableArea = load("res://objects/ladder/ladder.tscn").instance()
	level.add_child(climbableArea, true)
	
	level.remove_child(climbableTilemap)
	climbableArea.add_child(climbableTilemap)
	climbableTilemap.collision_use_parent = true
	
	
	handleConnectionsPost()
	
	# set camera limits
	var cam : Camera2D = level.player.camera
	
	cam.limit_left = int(cameraBounds.position.x)
	cam.limit_right = int(cameraBounds.end.x)
	cam.limit_top = int(cameraBounds.position.y)
	cam.limit_bottom = int(cameraBounds.end.y)
	
	canvasLayer.inventoryDisplay.inventoryToDraw = level.player.inventory
	
	
	emit_signal("levelLoaded")
	
	print("Loaded level '%s'" % _currentLevel)



# Set properties from a dictionary as metadata for an Node.
func set_properties_for_object(object:Object, properties: Dictionary):
	var include = ["type", "width", "height"]
	for key in properties:
		
		if object.get(key) != null or key in include:
			object.set(key, properties[key])
			#HACK: until we find a way to create object property
			object.set_meta(key, properties[key])

# Return a dictionary from a Node's metadata.
func meta_to_dict(object:Object) -> Dictionary:
	var properties: Dictionary = {}
	for property in object.get_meta_list():
		properties[property] = object.get_meta(property)
		
	return properties

# Move the player to a certain door/trigger on a certain level.
func move_player_to_door_on_level(source_door:Area2D, levelName:String, doorName:String, proportions: Vector2) -> void:
	var facing_right :bool = level.player.direction >= 0
	var moving_up    :bool = level.player.velocity.y < 0
	var right_of_door:bool = level.player.position.x > source_door.position.x
	
	# if level name isn't empty, load specified level
	if levelName != "":
		dropAndLoadLevel(levelName, ["move_player_to_door", [doorName, facing_right, right_of_door, moving_up, proportions]])
		yield(self, "levelLoaded")
	else:
		print("No level specified, using current")
		level.move_player_to_door(doorName, facing_right, right_of_door, moving_up, proportions)

# Reload current save on player death.
func on_player_death():
	dropLevel()
	load_game(currentSlot)
