#!/usr/bin/env python3
import sys
from pathlib import Path
import subprocess
import shutil

def cmd_exists(cmd):
	return shutil.which(cmd) is not None

args = sys.argv[1:]

level_path = args[0]
level_name = Path(level_path).stem
object_id = None
if len(args) > 1:
	object_id = args[1]

command = ["godot3.5", "-ul", level_name, "-us", "5"]
if object_id != None:
	command += ["--at", object_id]

if cmd_exists("godot3.5"):
	print(command)
	subprocess.run(command)
else:
	print("Please install godot v3.5.2 and make it available under godot3.5 in your PATH")
	sys.exit(1)
	