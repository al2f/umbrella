#!/usr/bin/env -S godot3.5 --script --no-window

extends SceneTree

const PATH = "build/"
const NAME = "umbrella"

const INCLUDE_FILTER = "*.tmx, *.tsx, *.tx, *.txt, *.json, *.cfg, *.mp3, *.ogg"
const EXCLUDE_FILTER = "package.py, build/"

const EXPORT_PRESETS = "res://export_presets.cfg"

func _set_value(configFile: ConfigFile, section: String, key: String, value):
	configFile.set_value(section, key, value)
	print("Set %s to %s under section %s" % [key, value, section])

func _init():
	var configFile := ConfigFile.new()
	var err = configFile.load(EXPORT_PRESETS)
	
	if err == OK:
		for section in configFile.get_sections():
			if section.begins_with("preset."):
				if not section.ends_with("options"):
					var presetName = configFile.get_value(section, "name")
					var use64Bit = presetName.ends_with("64")
					var presetPrefix = presetName.substr(0,3).to_lower()
					print(presetPrefix)
					var exportPath = PATH + NAME
					
					_set_value(configFile, section, "include_filter", INCLUDE_FILTER)
					_set_value(configFile, section, "exclude_filter", EXCLUDE_FILTER)
					
					match presetPrefix:
						"lin":
							_set_value(configFile, "%s.options" % section, "binary_format/64_bits", use64Bit)
							if use64Bit:
								_set_value(configFile, section, "export_path", exportPath + ".x86_64")
							else:
								_set_value(configFile, section, "export_path", exportPath + ".x86")
						"win":
							_set_value(configFile, "%s.options" % section, "binary_format/64_bits", use64Bit)
							if use64Bit:
								_set_value(configFile, section, "export_path", exportPath + "_64.exe")
							else:
								_set_value(configFile, section, "export_path", exportPath + "_32.exe")
			configFile.save(EXPORT_PRESETS)
								
		quit(0)
	else:
		printerr("Error %s when opening %s" % [err, EXPORT_PRESETS])
		quit(1)
