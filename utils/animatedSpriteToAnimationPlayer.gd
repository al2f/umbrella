tool
extends Node

export(NodePath) var spritePath
export(NodePath) var animationPlayerPath

func _ready():
	var sprite: AnimatedSprite = get_node(spritePath)
	var animationPlayer: AnimationPlayer = get_node(animationPlayerPath)
	
	var spriteFrames: SpriteFrames = sprite.frames
	for animName in spriteFrames.get_animation_names():
		print(animName)
		
		var fps = spriteFrames.get_animation_speed(animName)
		var frames = spriteFrames.get_frame_count(animName)
		var loop = spriteFrames.get_animation_loop(animName)
		
		var animation := Animation.new()
		animation.length = frames * (1.0/fps)
		animation.loop = loop
		
		var track_animation = animation.add_track(Animation.TYPE_VALUE)
		animation.track_set_path(track_animation, "playerSprite:animation")
		animation.track_insert_key(track_animation, 0.0, animName)
		
		var track_playing = animation.add_track(Animation.TYPE_VALUE)
		animation.track_set_path(track_playing, "playerSprite:playing")
		animation.track_insert_key(track_playing, 0.0, false)
		
		var track_frames = animation.add_track(Animation.TYPE_VALUE)
		animation.track_set_path(track_frames, "playerSprite:frame")
		
		for i in range(0, frames):
			animation.track_insert_key(track_frames, (1.0/fps)*i, i)
		
		var err = animationPlayer.add_animation(animName, animation)
		if err != OK:
			printerr(err)
