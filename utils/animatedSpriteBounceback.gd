extends AnimatedSprite

func play_bounceback(new_anim):
	var frameNum = frame
	var totalFrames = frames.get_frame_count(animation)
	playing = false
	animation = new_anim
	var totalFrames2 = frames.get_frame_count(new_anim)
	var newFrame = totalFrames2 - frameNum
	
	if not ([0, totalFrames].has(frameNum)):
		frame = newFrame
	playing = true
