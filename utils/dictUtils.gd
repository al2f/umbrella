extends Node
class_name DictUtils

#NOTE: Type Dictionary is not set as otherwise the return type of this function is
# inferred as a dictionary
static func get_dict_at_path(dict, path: Array):
	path = path.duplicate(true)
	
	var current_value = dict
	
	while path.size() >= 1:
		if current_value is Dictionary:
			var key = path.pop_front()
			current_value = current_value.get(key)
			if current_value == null:
				return current_value
	return current_value

static func get_dict_at_path_without_reference(dict, path: Array):
	dict = dict.duplicate(true)
	
	return get_dict_at_path(dict, path)

static func merge_recursive(dest: Dictionary, source: Dictionary) -> void:
	for key in source:
		if source[key] is Dictionary:
			if not dest.has(key):
				dest[key] = {}
			merge_recursive(dest[key], source[key])
		else:
			dest[key] = source[key]
