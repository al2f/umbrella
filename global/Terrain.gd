class_name Terrain
extends Node

static func get_tile_type(tile_id: int):
	if tile_id == -1:
		return ""
	elif tile_id > 449:
		return "grass"
	else:
		return "stone"

static func get_land_sound_for_type(type: String):
	if type == "grass":
		var num = (randi()%2) + 1
		return "kdd_different_steps/leaves0%d.ogg" % num
	elif type == "stone":
		return "kdd_different_steps/stone01.ogg"
