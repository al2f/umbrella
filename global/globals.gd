extends Node

signal screenCaptured

# counter used to get time passed since game launch
var time = 0

func _ready():
	pass

# increment counter
func _process(delta):
	time += delta

func swapKeyValues(dict: Dictionary):
	var newDict = {}
	for key in dict.keys():
		var value = dict[key]
		newDict[value] = key
	return newDict
	
func captureScreen(path: String):
	var prevClearMode = get_viewport().render_target_clear_mode
	var prevUpdateMode = get_viewport().render_target_update_mode
	
	get_viewport().render_target_clear_mode = Viewport.CLEAR_MODE_ONLY_NEXT_FRAME
	get_viewport().render_target_update_mode = Viewport.UPDATE_DISABLED
	# Wait until the frame has finished before getting the texture.
	yield(VisualServer, "frame_post_draw")
	# Retrieve the captured image.
	var img = get_viewport().get_texture().get_data()
	# Flip it on the y-axis (because it's flipped).
	img.flip_y()
	img.save_png(path)
	get_viewport().render_target_clear_mode = prevClearMode
	get_viewport().render_target_update_mode = prevUpdateMode
	
	emit_signal("screenCaptured")

func get_screenshot_path(idx: int) -> String:
	return "user://shot%d.png" % idx

func load_external_tex(imagePath) -> Texture:
	var texture = ImageTexture.new()
	texture.flags = Texture.FLAG_MIPMAPS + Texture.FLAG_REPEAT 
	var image = Image.new()
	image.load(imagePath)
	texture.create_from_image(image)
	return texture
	#func load_external_tex(path):
#	# https://libredd.it/r/godot/comments/ox381i/how_can_i_load_a_image_from_my_filesystem_into_my/
#	var tex_file = File.new()
#	var err = tex_file.open(path, File.READ)
#	if err == OK:
#		var bytes = tex_file.get_buffer(tex_file.get_len())
#		var img = Image.new()
#		var data = img.load_png_from_buffer(bytes)
#		var imgtex = ImageTexture.new()
#		imgtex.create_from_image(img)
#		tex_file.close()
#		return imgtex
#	else:
#		push_error("error loading external texture: " + path)
#		get_tree().quit(1)

func resolve_dot_import(path):
	path = path.simplify_path()
	
	var dir := Directory.new()
	
	if dir.file_exists(path):
		return path
	else:
		var importPath: String = path + ".import"
		if dir.file_exists(importPath):
			var configFile = ConfigFile.new()
			var err = configFile.load(importPath)
			if err != OK:
				printerr(err, "Error when loading import file %s: " % importPath)
			else:
				var remapPath: String = configFile.get_value("remap", "path")
				if dir.file_exists(remapPath):
					return remapPath
				
				printerr("Invalid path to remapped file in %s: %s" % [importPath, remapPath])
		else:
			printerr(".import file for %s not found" % [path])
