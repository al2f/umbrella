extends Node

var settings =  {
	"": {
		"buttons": ["titlescreen", "controls", "credits"],
		"submenus": ["video", "audio", "files", "debug"],
		"options": {},
		"hidden": []
		
	},
	"video": {
		"buttons": [],
		"submenus": [],
		"options": {
			"fullscreen": {
				"type": "checkbutt",
				"value": true
			},
		},
		"hidden": []
	},
	"audio": {
		"buttons": [],
		"submenus": [],
		"options": {
			"mute_sfx": {
				"type": "checkbutt",
				"value": false
			},
			"vol_sfx": {
				"type": "slider",
				"min": 0,
				"max": 1,
				"step": 0.05,
				"value": 1
			},
			"sep1": {"type": "separator"},
			"mute_mus": {
				"type": "checkbutt",
				"value": false
			},
			"vol_mus": {
				"type": "slider",
				"min": 0,
				"max": 1,
				"step": 0.05,
				"value": 1
			},
			"sep2": {"type": "separator"},
			"mute_ui": {
				"type": "checkbutt",
				"value": false
			},
			"vol_ui": {
				"type": "slider",
				"min": 0,
				"max": 1,
				"step": 0.05,
				"value": 1
			}
		}
	},
	"files": {
		"buttons": ["open_save_folder", "open_save"],
		"submenus": [],
		"options": {},
		"hidden": []
	},
	"debug": {
		"buttons": [],
		"submenus": [],
		"options": {
			"debug_on_start": {
				"type": "checkbutt",
				"value": false
			},
		},
		"hidden": [] 
	}
}

var settingsPath := "user://settings.cfg"

var debug = false setget set_debug
var options = {
	"slot": null,
	"level": null,
	"custom_position": null,
	"spawn_at_name": null
}

var baseInputMap := {}

var saveBuffer := {"levels":{}, "general":{}}

var bushData := []
var saveData := saveBuffer.duplicate(true)
var itemData := {}
var objectTypesData := {}

const VERSION = "0.1.0"
var commit := ""
var tag := ""
var commits_since := 0

func clearSaveInfo() -> void:
	saveBuffer["levels"] = {}
	saveBuffer["general"] = {}
	saveData["levels"] = {}
	saveData["general"] = {}

func usage():
	print(tr("TERMINAL_USAGE"))
	print(tr("TERMINAL_ARGUMENT_NOTES"))

func get_build_info():
	var res = ""
	res += tr("TERMINAL_VERSION") % VERSION + "\n"
	res += tr("TERMINAL_COMMIT") % commit + "\n"
	res += tr("TERMINAL_COMMITS_SINCE") % [commits_since, tag] + "\n"
	return res

func set_debug(value):
	debug = value
	LevelManager.canvasLayer.gui.get_node("HBoxContainer/debugIndicator").visible = debug
	var sm = LevelManager.canvasLayer.settingsMenu
	
	sm.menu.set_option_visible([], "debug", debug)

# Called when the node enters the scene tree for the first time.
func _ready():
	print(ProjectSettings.get("application/config/name"))
	
	usage()

	print("-")
	
	var cfg := ConfigFile.new()
	var err = cfg.load("res://hash.txt")
	if err == OK:
		commit = cfg.get_value("main", "commit", "0000000")
		tag = cfg.get_value("main", "tag", "invalid")
		commits_since = cfg.get_value("main", "since", NAN)
	
	print(get_build_info())
	
	print("-\n\n\n")
	baseInputMap = getKeys()
	
	
	
	bushData = configFileToDict("res://items/bushes.cfg")["items"]["bushes"]
	itemData = configFileToDict("res://items/items.cfg")
#	objectTypesData = getObjectTypes()
#
#	if createIfNotExists(keyPath):
#		writeKeys()
#	applyKeys()
	
	handle_args(OS.get_cmdline_args())
	yield(LevelManager, "ready")
	
	# Set default settings for settingsMenu
	setSettings(settings)
	
	# Create file if it doesn't exist, and fill with default values
	if createIfNotExists(settingsPath):
		writeSettings()
	applySettings()
	
	if createIfNotExists(ControlsManager.CONTROLS_SAVE_PATH):
		ControlsManager.save_controls()
	
	if LevelManager.canvasLayer.settingsMenu.menu.geto("debug/debug_on_start"):
		self.debug = true

func handle_args(args: PoolStringArray):
	var unparsed_args = Array(args)
	unparsed_args.invert()
	
	while len(unparsed_args) != 0:
		var arg = unparsed_args.pop_back()
		match arg:
			"--slot", "-us":
				var slot = unparsed_args.pop_back()
				if slot == null:
					push_error("ERROR: Must provide slot number for use with '%s'" % arg)
					get_tree().quit(1)
				elif slot.is_valid_integer():
					options["slot"] = int(slot)
				else:
					push_error("ERROR: Slot number for '%s' must be an integer" % arg)
			"--level", "-ul":
				options["level"] = unparsed_args.pop_back()
			"--spawn":
				options["custom_position"] = Vector2(unparsed_args.pop_back(), unparsed_args.pop_back())
			"--at":
				options["spawn_at_name"] = unparsed_args.pop_back()
	if options.get("level") != null and options.get("slot") == null:
		push_error("ERROR: '--load-level' requires '--slot'")
# Functions for reading/writing/getting/applying settings
# reading: read from file
# writing: write to file
# setting: set value of variable
# getting: get from game
# apply: read from file and set

func getSettings():
	return LevelManager.canvasLayer.settingsMenu.menu.export_data()

func setSettings(value):
	settings = value
	LevelManager.canvasLayer.settingsMenu.menu.set_data(value)

func writeSettings():
	writeFile(settingsPath, JSON.print(getSettings(), "\t"))

func applySettings():
	var result = readFile(settingsPath)
	if result != null and result is String:
		var json_result = JSON.parse(result)
		if json_result.error == OK:
			var data = json_result.result
			if data != null:
				LevelManager.canvasLayer.settingsMenu.menu.add_to_data(data)

func getKeys():
	var dictionary = {}
	
	for actionName in InputMap.get_actions():
		dictionary[actionName] = []
		for event in InputMap.get_action_list(actionName):
			dictionary[actionName].append(eventToDict(event))
	
	return dictionary

func setKeys(keys: Dictionary):
	
	for action in keys:
		if not InputMap.has_action(action):
			continue
		InputMap.action_erase_events(action)
		
		var event = null
		for key in keys[action]:
			if key["type"] == "key":
				event = InputEventKey.new()
				event.scancode = key["button"]
			elif key["type"] == "mouse":
				event = InputEventMouseButton.new()
				event.button_index = key["button"]
			elif key["type"] == "joyBtn":
				event = InputEventJoypadButton.new()
				event.button_index = key["button"]
			elif key["type"] == "joyAxis":
				event = InputEventJoypadMotion.new()
				event.axis = key["axis"]
				event.axis_value = key["axis_value"]
			
			InputMap.action_add_event(action, event)



################################################################################



func writeFile(path: String, content: String):
	var file = File.new()
	var err = file.open(path, File.WRITE)
	if err == OK:
		file.store_string(content)
		file.close()
		return OK
	else:
		Logger.print_err(err, "Failed to write file %s " % ProjectSettings.globalize_path(path))
		return err
		
func readFile(path: String):
	var file = File.new()
	var err = file.open(path, File.READ)
	if err == OK:
		var content = file.get_as_text()
		file.close()
		return content
	else:
		Logger.print_err(err, "Failed to read file %s " % ProjectSettings.globalize_path(path))
		return err

# create file at path if it doesn't exist
# returns 'true' if file was created
# returns 'false' if file already existed
func createIfNotExists(path: String):
	var directory := Directory.new()
	if directory.file_exists(path):
		return false
	else:
		print("Creating %s" % ProjectSettings.globalize_path(path))
		var file = File.new()
		var err = file.open(path, File.WRITE)
		
		if err == OK:
			file.close()
			return true
		else:
			Logger.print_err(err, "Failed to create file %s " % ProjectSettings.globalize_path(path))
		

func eventToDict(event: InputEvent):
	match event.get_class():
		"InputEventKey":
			return {
				"type": "key",
				"button": event.get_scancode_with_modifiers()
			}
		"InputEventMouseButton":
			return {
				"type": "mouse",
				"button": event.button_index
			}
		"InputEventJoypadButton":
			return {
				"type": "joyBtn",
				"button": event.button_index
			}
		"InputEventJoypadMotion":
			return {
				"type": "joyAxis",
				"axis": event.axis,
				"axis_value": event.axis_value
			}

func configFileToDict(filePath: String):
	var configFile := ConfigFile.new()
	var err = configFile.load(filePath)
	if err != OK:
		Logger.print_err(err, "Error when loading %s" % ProjectSettings.globalize_path(filePath))
		get_tree().quit(1)
	var results = {}
	
	for section in configFile.get_sections():
		results[section] = {}
		for key in configFile.get_section_keys(section):
			results[section][key] = configFile.get_value(section, key)
	
	return results

func get_save_file_path(slot: int):
	return str("user://save", slot, ".cfg")

func get_save_screenshot_path(slot: int):
	return str("user://save", slot, ".png")

func save_to_dict(slot: int) -> Dictionary:
	var saveFilePath = get_save_file_path(slot)
	var results: Dictionary = configFileToDict(saveFilePath)
	return results

func storeStats(object: Object):
	if not saveBuffer["levels"].has(LevelManager.getLevelName()):
		saveBuffer["levels"][LevelManager.getLevelName()] = {}
		
	var stats = object.get_stats()
	saveBuffer["levels"][LevelManager.getLevelName()][object.name] = stats
	print(" - set %s's stats to %s" % [object.name, stats])

func restoreStats(object: Object):
	var stats = getStatsOrNull(LevelManager.getLevelName(), object)
	if stats != null:
		print(" - restored %s's stats of %s" % [object.name, stats])
		object.set_stats(stats)
	
func getStatsOrNull(levelName: String, object: Object):
	# checking for info in save buffer
	var tempLevelStats = saveBuffer["levels"].get(levelName)
	if tempLevelStats != null:
		var data = tempLevelStats.get(object.name)
		if data != null:
			print("Found data in save buffer for object %s" % object)
			return data

	# checking for info in save data
	var levelStats = saveData["levels"].get(levelName)
	if levelStats != null:
		var data = levelStats.get(object.name)
		if data != null:
			print("Found data in save data(from disk)")
			return data
	
	return null

func getObjectTypes() -> Dictionary:
	var file = File.new()
	file.open("res://tiled/objecttypes.json", File.READ)
	var content = file.get_as_text()
	file.close()
	
	var result := JSON.parse(content)
	if result.error != OK:
		Logger.print_err(result.error, "Error on line %d when parsing res://tiled/objecttypes.json: %s" % [result.error_line, result.error_string])
	else:
		var output := {}
#		print(result.result)
		for objectClass in result.result:
			var objectName: String = objectClass["name"]
			output[objectName] = {}
			
			for objProp in objectClass["members"]:
				var propName: String = objProp["name"]
				
				var value = objProp["value"]
				match objProp["type"]:
					"int":
						output[objectName][propName] = int(value)
					"float":
						output[objectName][propName] = float(value)
					"bool":
						output[objectName][propName] = bool(value)
					"color":
						output[objectName][propName] = Color(value)
					"string", "file":
						output[objectName][propName] = String(value)
					_:
						output[objectName][propName] = String(value)
		return output
	return {}


