extends Node

var pos_players = []
var nonpos_players = []

var used_pos_players = []
var used_nonpos_players = []

const DEFAULT = {
	"volume": 1,
	"pitch_min": 0.7,
	"pitch_max": 1.3,
}
const SETTINGS = {
	"button_check.mp3":
		{
			"volume": 0.1
		},
	"button_uncheck.mp3":
		{
			"volume": 0.1
		},
	"sfx_breaking_and_falling/bfh1_rock_falling_06.ogg":
		{
			"volume": 0.5
		}
}

func player_play_sound(player, path: String, bus_name:String, priority_settings: Dictionary = {}):
	# For future, if needed
#	player.pause_mode = (PAUSE_MODE_PROCESS if bus_name == "ui" else PAUSE_MODE_STOP)
	
	var snd_file = Global.resolve_dot_import("res://sounds/" + path)
	if snd_file == null:
		return
	
	var f = File.new()
	
	if f.file_exists(snd_file):
		var sound = load(snd_file)
		player.stream = sound
		if player.stream is AudioStreamMP3:
			player.stream.loop = false
		elif player.stream is AudioStreamOGGVorbis:
			player.stream.loop = false
		
		var settings = priority_settings.duplicate(true)
		
		# Get missing keys from first the sound-specific file settings, and then
		# from the global default
		settings.merge(SETTINGS.get(path, {}))
		settings.merge(DEFAULT)
		
		var vol = settings["volume"]
		
		player.volume_db = linear2db(vol)
		
		var pitch_min = settings["pitch_min"]
		var pitch_max = settings["pitch_max"]
		player.pitch_scale = rand_range(pitch_min, pitch_max)
		
		player.bus = bus_name
		player.play()
	else:
		player.emit_signal("finished")
		push_warning("Sound '%s' does not exist." % snd_file)


func _ready():
	pause_mode = PAUSE_MODE_PROCESS
	
	for i in range(5):
		var sp = AudioStreamPlayer2D.new()
		pos_players.append(sp)
		sp.connect("finished", self, "on_pos_player_finished", [sp])
		add_child(sp)
		
		var nsp = AudioStreamPlayer.new()
		nonpos_players.append(nsp)
		nsp.connect("finished", self, "on_nonpos_player_finished", [nsp])
		add_child(nsp)

func on_nonpos_player_finished(player):
	used_nonpos_players.erase(player)
	nonpos_players.append(player)

func on_pos_player_finished(player):
	used_pos_players.erase(player)
	pos_players.append(player)

func play_sound(snd: String, bank_name: String, settings: Dictionary = {}):
	if nonpos_players.empty():
		print_debug("Non-positional player queue full!")
		return
	var player: AudioStreamPlayer = nonpos_players.pop_back()
	used_nonpos_players.append(player)
	
	player_play_sound(player, snd, bank_name, settings)


func play_sound_at_pos(snd: String, bank_name: String, pos: Vector2, settings: Dictionary = {}):
	if pos_players.empty():
		print_debug("Positional player queue full!")
		return
	var player: AudioStreamPlayer2D = pos_players.pop_back()
	used_pos_players.append(player)
	
	player.global_position = pos
	player_play_sound(player, snd, bank_name, settings)
