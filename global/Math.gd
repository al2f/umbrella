extends Node

func clampi(value: int, minimum: int, maximum: int):
	if value < minimum:
		return minimum
	if value > maximum:
		return maximum
	return value

func sum(arr: Array):
	var result = 0
	for item in arr:
		result += item
	return result

func average(arr: Array):
	if len(arr) == 0:
		return arr[0]
	return sum(arr)/len(arr)
