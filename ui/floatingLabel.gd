extends HBoxContainer

enum Types {
	HEALTH_CHANGE,
	HEALTH_CHANGE_NPC,
	ITEM,
	OTHER
}
#var TEXTURES = [
#	load("res://ui/heart2.png"),
#	load("res://ui/heart.png"),
#	null,
#	null
#]
var type = Types.OTHER

var data

onready var tween := get_node("Tween")
onready var label := get_node("Label")
onready var textureRect := get_node("TextureRect")

# Called when the node enters the scene tree for the first time.
func _ready():
	match type:
		Types.HEALTH_CHANGE, Types.HEALTH_CHANGE_NPC:
			if typeof(data) != TYPE_INT:
				push_error("Data provided for floating label of type 'health change'  '%s' was not an integer" % data)
			
			if data >= 0:
				label.add_color_override("font_color", Color.green)
				label.text = tr("FLABEL_HEAL") % data + " "
			elif data < 0:
				if type == Types.HEALTH_CHANGE_NPC:
					label.add_color_override("font_color", Color.orange)
				else:
					label.add_color_override("font_color", Color.crimson)
				label.text = tr("FLABEL_DAMAGE") % abs(data) + " "
		
		Types.ITEM:
			label.add_color_override("font_color", Color.wheat)
			label.text = tr("FLABEL_HEAL") % data["amount"]
			textureRect.texture = load("res://items/%03d.png" % data["id"])
		
	tween.interpolate_property(self, "modulate:a", 1, 0, 2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 2)
	tween.interpolate_property(self, "rect_position", rect_position, rect_position-Vector2(0,32), 1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	
	#warning-ignore:return_value_discarded
	tween.connect("tween_all_completed", self, "on_animation_finished")
	
func on_animation_finished():
	queue_free()
	
