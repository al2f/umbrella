extends VBoxContainer

var messageScene = preload("res://ui/message.tscn")

var themes = {
	"normal": "res://ui/messageNormal.tres",
	"error": "res://ui/messageError.tres"
}
func _ready():
	pass

func add_message(text: String = "Test Message", styleName: String = "normal", timeout: float = -1):
	var newMessage = messageScene.instance()
	newMessage.text = text
	newMessage.timeout = timeout
	newMessage.theme = load(themes[styleName])
	
	add_child(newMessage)
	
	if get_child_count() > 3:
		get_child(0).queue_free()
