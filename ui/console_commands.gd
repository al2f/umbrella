extends Object

static func get_object_from_path(node: Node, path: String) -> Array:
	var res = node.get_node_or_null(path)
	if res == null:
		return [null, "Node not found: %s" % path]
	return [res, null]

static func get_object_from_prop(instance: Object, prop: String) -> Array:
	var res = instance.get(prop)
	if res is Object:
		return [res, null]
	return [null, "%s's property %s is not an object" % [instance.get_path(), prop]]

static func get_node_or_prop(instance: Object, path: String) -> Array:
	if instance is Node:
		return get_object_from_path(instance, path)
	return get_object_from_prop(instance, path)

static func methods(obj: Object) -> Array:
	var res = []
	for meth in obj.get_method_list():
		if meth.flags & METHOD_FLAG_FROM_SCRIPT == METHOD_FLAG_FROM_SCRIPT:
			var bit = meth.name
			if len(meth.args) != 0:
				bit += " %s" % len(meth.args)
			res.append(bit)
	return [res, null]

static func props(obj: Object) -> Array:
	var res = []
	for prop in obj.get_property_list():
		if prop.usage & PROPERTY_USAGE_SCRIPT_VARIABLE == PROPERTY_USAGE_SCRIPT_VARIABLE:
			res.append(prop.name)
	return [res, null]

static func props_all(obj: Object) -> Array:
	var res = []
	for prop in obj.get_property_list():
		res.append(prop.name)
	return [res, null]
