extends PanelContainer

onready var label := get_node("Label")
onready var centerContainer := get_node("CenterContainer")

func setAmount(amount: int):
	label.text = str(amount)

func clearAmount():
	label.text = ""

func setItem(itemId: int):
	centerContainer.get_node("TextureRect").texture = load("res://items/%03d.png" % itemId)
	
func clearItem():
	centerContainer.get_node("TextureRect").texture = null
