extends TabContainer

func _ready() -> void:
	connect("tab_selected", self, "on_tab_selected")

func on_tab_selected(tab: int):
	get_child(tab).grab_focus()
