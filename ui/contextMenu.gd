extends PopupMenu

#HACK: workaround for https://github.com/godotengine/godot-proposals/issues/2663
# PopupMenu cannot be easily navigated using keyboard

func popup(rect = Rect2(0,0,0,0)):
	set_current_index(0)
	.popup(rect)

func _input(event):
	if visible:
		
		accept_event()
		
		if event.is_action("ui_up") or event.is_action("ui_down"):
			
			var direction := 0
			
			get_tree().set_input_as_handled()
			
			if event.is_action_pressed("ui_down"):
				direction = 1
			elif event.is_action_pressed("ui_up"):
				direction = -1
			
			var new_index := get_current_index()
			new_index = wrapi(new_index + direction, 0, get_item_count())
			set_current_index(new_index)
		elif event.is_action_pressed("ui_accept"):
			accept_event()
			if is_item_disabled(get_current_index()):
				return

			emit_signal("index_pressed", get_current_index())
			emit_signal("id_pressed", get_item_id(get_current_index()))
			visible = false
		elif event.is_action_pressed("ui_cancel"):
			accept_event()
			
			visible = false
