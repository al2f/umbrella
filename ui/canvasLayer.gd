extends CanvasLayer

onready var gui = get_node("gui")
onready var heartContainer = gui.get_node("HBoxContainer/hearts")
onready var heartSize = heartContainer.get_node("full").texture.get_size()
onready var inventoryDisplay = gui.get_node("inventoryDisplay")

onready var npcLabel := get_node("npcLabel")

onready var settingsMenu := get_node("menu")
onready var messageContainer := get_node("messageContainer")
#onready var debugConsole := get_node("debugConsole")

func _ready():
	#warning-ignore:return_value_discarded
	settingsMenu.connect("option_changed", self, "on_menu_option_changed")
	
	yield(LevelManager, "levelLoaded")
	inventoryDisplay.hide()
	
	settingsMenu.menu.navigate_path(".")
	

func show_label(text):
	npcLabel.show()
	npcLabel.text = text

func force_hide_label():
	npcLabel.hide()

func hide_label(text):
	if npcLabel.text == text:
		npcLabel.hide()

func on_menu_option_changed(value, path: Array, option_name: String):
	if value != null:
		print("Setting %s to %s" % [ "> ".join(path) + " > " + tr("MENU_%s" % option_name.to_upper()), value])
		Config.settings = settingsMenu.menu.get_data()
		
		if value is bool:
			SoundManager.play_sound("button_check.mp3" if value else "button_uncheck.mp3", "ui")
		match option_name:
			"vol_sfx":
				SoundManager.play_sound("kdd_different_steps/stone01.ogg", "sfx")
			"vol_ui":
				SoundManager.play_sound("button_check.mp3", "ui")
	if path != [] and option_name == "back":
		settingsMenu.menu.navigate_path("..")
		return
	
	match path:
		[]:
			match option_name:
				"back":
					settingsMenu.hide()
					Config.writeSettings()
					if inventoryDisplay.visible:
						inventoryDisplay.on_visibility_changed()
					get_tree().set_input_as_handled()
					get_tree().paused = false
				"controls":
					settingsMenu.get_node("input").popup_centered_ratio(1)
				"credits":
					$menu/credits.popup_centered_ratio(1)
				"titlescreen":
					get_tree().paused = false
					LevelManager.currentSlot = -1
					LevelManager.dropLevel()
					
					# ensure that data does not leak into next loaded save
					Config.clearSaveInfo()
					inventoryDisplay.hide()
					LevelManager.loadTiledLevel("titlescreen")
					
					settingsMenu.hide()
		["video"]:
			match option_name:
				"fullscreen":
					OS.window_fullscreen = value
					OS.window_borderless = false
		["audio"]:
			if option_name.begins_with("mute_"):
				var bank_name = option_name.trim_prefix("mute_")
				var bus_idx = AudioServer.get_bus_index(bank_name)
				
				if bus_idx != -1:
					AudioServer.set_bus_mute(bus_idx, value)
					print("Bus name: %s\nvalue: %s" % [bank_name, value])
			elif option_name.begins_with("vol_"):
				var bank_name = option_name.trim_prefix("vol_")
				var bus_idx = AudioServer.get_bus_index(bank_name)
				if bus_idx != -1:
					AudioServer.set_bus_volume_db(bus_idx, linear2db(value))
					print("Bus name: %s\nvalue: %s" % [bank_name, value])
		["files"]:
			match option_name:
				"open_save":
					var file_path = ProjectSettings.globalize_path(Config.get_save_file_path(LevelManager.currentSlot))
					var err = OS.shell_open(file_path)
					if err != OK:
						Logger.print_err(err, "Failed to open '%s'" % file_path)
				"open_save_folder":
					var file_path = ProjectSettings.globalize_path("user://")
					var err = OS.shell_open(file_path)
					if err != OK:
						Logger.print_err(err, "Failed to open '%s'" % file_path)
#		"console":
#			settingsMenu.hide()
#			debugConsole.popup_centered_ratio(1)

func show_settings():
	get_tree().paused = true
	settingsMenu.show()
	settingsMenu.backButton.grab_focus()
	
	get_tree().set_input_as_handled()
	
func _input(event) -> void:
#	if event.is_action_released("debug_console"):
#		if not debugConsole.visible:
#			debugConsole.popup_centered_ratio(1)
#		else:
#			debugConsole.hide()
#		get_tree().set_input_as_handled()
	if not settingsMenu.visible:
		if event.is_action_pressed("settings"):
			show_settings()
		
		if event.is_action_pressed("inventory"):
			if not inventoryDisplay.visible:
				if LevelManager.level.player.is_on_floor() or Config.debug:
					inventoryDisplay.show()
					get_tree().set_input_as_handled()
	elif settingsMenu.visible:
		#HACK: Should use _gui_input to ensure that the more 'on top' control
		# rebinding menu gets the event, and we don't close while keeping it open
		
		if not settingsMenu.get_node("input").visible:
			if event.is_action_pressed("settings"):
				on_menu_option_changed(null, settingsMenu.menu.current_path, "back")
#				settingsMenu.on_menu_button_pressed([], "back")

# update heart display when players' health changes
# this is connected by game.gd
func update_hearts(health: int, maxHealth: int):
	heartContainer.get_node("empty").rect_size.x = heartSize.x*maxHealth
	heartContainer.get_node("full").rect_size.x = heartSize.x*health
	heartContainer.get_node("full").visible = !(health <= 0)

func on_inventoryFull(itemName: String, _amount: int):
	messageContainer.add_message(tr("MESSAGE_INVENTORY_FULL") % itemName)
