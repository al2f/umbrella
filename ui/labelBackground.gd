extends PanelContainer

onready var label := get_node("Label")

var text:= "" setget set_text

func set_text(value):
	text = value
	if is_inside_tree():
		label.text = text
