extends ColorRect

# flash green when key is pressed then fade to white
# flash red when released then fade to black

func _process(_delta):
	if Input.is_action_pressed(name):
		color = color.lightened(0.1)
	else:
		color = color.darkened(0.05)

func _input(event):
	if event.is_action_pressed(name):
		color = Color.green
	elif event.is_action_released(name):
		color = Color.red
