extends Label

var timeout: float setget set_timeout
var style: Theme setget set_theme

func _ready():
	#warning-ignore:return_value_discarded
	$Timer.connect("timeout", self, "queue_free")
	$Timer.start()

func set_timeout(value: float):
	if value != -1:
		timeout = value
		$Timer.wait_time = timeout

func set_theme(value: Theme):
	style = value
