extends PopupPanel

onready var commandInput := get_node("VBox/VBox/LineEdit")
onready var commandOutput := get_node("VBox/ScrollContainer/RichTextLabel")
onready var targetLabel := get_node("VBox/VBox/Label")

onready var command_target: Object setget set_command_target
var target_node_path := ""

var COMMANDS := load("res://ui/console_commands.gd")

# exit status of last wrapped program
var qu: int
# result of last wrapped program
var re

const COMMAND_HELP = [
	["help", "show help"],
	["children", "show children of selected node"],
	["parent", "show parent of selected node"],
	["props", "list selected object's properties"],
	["methods", "list selected object's methods followed by the number of arguments it accepts"],
	["expr|e", "evaluate gdscript expression"],
	["enter|select path|prop", "select object for future commands."],
	["get prop", "get property from selected object"],
	["exit", "close debug console"],
]

func set_command_target(target: Object):
	command_target = target
	
	targetLabel.text = get_string_reference_for_obj(target)
	
	if target is Node:
		target_node_path = target.get_path()
		return

func _ready():
	commandInput.connect("text_entered", self, "on_command_entered")
	connect("visibility_changed", self, "on_visibility_changed")
	commandOutput.connect("meta_clicked", self, "on_link_clicked")
	yield(LevelManager, "levelLoaded")
	self.command_target = get_node("/root/level")
	
	execute_command('help')

func on_link_clicked(link: String):
	var data = link.split(" ")
	match data[0]:
		"object":
			var obj_id = int(data[1])
			self.command_target = instance_from_id(obj_id)
			
		"property":
			var obj_id = int(data[1])
			var obj = instance_from_id(obj_id)
			
			var prop_name = data[2]
			
			show_command_echoed("get %s" % prop_name)
			show_command_output(obj.get(prop_name))

func on_visibility_changed():
	if visible:
		commandInput.grab_focus()

func on_command_entered(new_text: String):
	execute_command(new_text)

func bb_link_from_obj(obj: Object):
	var data = "object %s" % obj.get_instance_id()
	return "[url=%s]%s[/url]" % [data, str(obj)]

func bb_link_from_prop(obj: Object, prop: String):
	var data = "property %s %s" % [obj.get_instance_id(), prop]
	return "[url=%s]%s[/url]" % [data, prop]
	
func execute_command(command: String):
	var parts := command.split(' ')
	
	var expression = Expression.new()
	var rest = command.trim_prefix(parts[0] + " ")
	
	commandInput.text = ""
	
	if not is_instance_valid(command_target):
		print("Command target is invalid instance")
		show_command_echoed("Notice")
		show_command_output("Selected object was removed!")
		var node = get_node_or_null(target_node_path)
		if node != null:
			show_command_output("Found it again.")
			self.command_target = node
		else:
			show_command_output("Could not find the object. Falling back to /root/level")
			self.command_target = get_node("/root/level")
			
	match parts[0]:
		"exit":
			hide()
		"help":
			show_command_echoed(command)
			for command in COMMAND_HELP:
				show_command_output("%s\t\t%s" % [command[0], command[1]])
		"children":
			if command_target is Node:
				show_command_echoed(command)
				for child in command_target.get_children():
					show_command_output(child)
		"parent":
			if command_target is Node:
				show_command_echoed(command)
				show_command_output(command_target.get_parent())
		"expr", "e":
			show_command_echoed(rest)
			expr(rest, command_target)
		"enter", "select":
			show_command_echoed(command)
			var node_path = rest.split(" ")[0]
			
			wrap(COMMANDS.get_node_or_prop(command_target, node_path))
			if qu == OK:
				self.command_target = re
				show_command_output(re)
		"methods":
			show_command_echoed(command)
			show_command_output(debug_show_methods(command_target))
		"methods_all":
			show_command_echoed(command)
			show_command_output(debug_show_methods_all(command_target))
		"props":
			show_command_echoed(command)
			wrap(COMMANDS.props(command_target))
			if qu == OK:
				for prop in re:
					show_command_output(bb_link_from_prop(command_target, prop))
		"props_all":
			show_command_echoed(command)
			wrap(COMMANDS.props_all(command_target))
			if qu == OK:
				for prop in re:
					show_command_output(bb_link_from_prop(command_target, prop))
		"get":
			show_command_echoed(command)
			var prop = rest.split(" ")[0]
			if prop in command_target:
				show_command_output(command_target.get(prop))
			else:
				show_command_error("%s does not have '%s' property" % [command_target, prop])
		_:
			if command in debug_show_properties_all(command_target):
				show_command_echoed(command)
				expr(command, command_target)
				return
			show_command_echoed(command)
			show_command_error("Unrecognised command %s" % command)

func get_string_reference_for_obj(obj: Object):
	if obj is Node:
		return obj.get_path()
	else:
		return str(obj)

func _unhandled_input(event):
	if event.is_action_released("debug_console"):
		visible = !visible
		get_tree().paused = !get_tree().paused
		
		if visible:
			commandInput.grab_focus()
		get_tree().set_input_as_handled()


func expr(command, target: Object):
	var expression := Expression.new()
	var error = expression.parse(command, [])
	if error != OK:
		show_command_error(expression.get_error_text())
		return
	var result = expression.execute([], target, true)
	print(result)
	if not expression.has_execute_failed():
		if result != null:
			show_command_output(result)

func show_command_echoed(output):
	show_text_coloured("\n> " + str(output), Color.gray)
func show_command_error(output):
	show_text_coloured("\n" + str(output), Color.red)

func show_text_coloured(output: String, color: Color = Color.white):
	commandOutput.push_color(color)
	commandOutput.add_text(output)
	commandOutput.pop()

func show_command_output(output):
	if not (output is String):
		output = str(output)
	var regex := RegEx.new()
	regex.compile("([^.@|\"%]+):\\[([A-Za-z0-9]+):([0-9]+)\\]")
	var matches = regex.search_all(output)
	for m in matches:
		
		var subject = m.strings[0]
		var node_name = m.strings[1]
		var node_class = m.strings[2]
		var node_id = int(m.strings[3])
		output = output.replace(subject, bb_link_from_obj(instance_from_id(node_id)))
	if output is String:
		commandOutput.append_bbcode("\n" + output)
		return
	commandOutput.add_text("\n" + JSON.print(output, "\t"))

func wrap(result):
	if result[0] == null:
		qu = FAILED
		re = result[1]
		show_command_error(re)
		return
	qu = OK
	re = result[0]

func debug_show_methods(obj: Object):
	var res = []
	for meth in obj.get_method_list():
		if meth.flags & METHOD_FLAG_FROM_SCRIPT == METHOD_FLAG_FROM_SCRIPT:
			var bit = meth.name
			if len(meth.args) != 0:
				bit += " %s" % len(meth.args)
			res.append(bit)
	return res

func debug_show_methods_all(obj: Object):
	var res = []
	for meth in obj.get_method_list():
		var bit = meth.name
		if len(meth.args) != 0:
			bit += " %s" % len(meth.args)
		res.append(bit)
	return res

func debug_show_properties(obj: Object):
	var res = []
	for prop in obj.get_property_list():
		if prop.usage & PROPERTY_USAGE_SCRIPT_VARIABLE == PROPERTY_USAGE_SCRIPT_VARIABLE:
			res.append(prop.name)
	return res

func debug_show_properties_all(obj: Object):
	var res = []
	for prop in obj.get_property_list():
		res.append(prop.name)
	return res
