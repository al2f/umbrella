extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var gridContainer := get_node("VBoxContainer/HBoxContainer/GridContainer")
onready var cursor := get_node("cursor")
onready var descLabel := get_node("VBoxContainer/Label")
onready var statLabel := get_node("VBoxContainer/BBLabel")

onready var itemMenu := get_node("contextMenu")

var inventoryItemScene = load("res://ui/inventoryItem.tscn")
var inventoryToDraw : Inventory

enum MODES {
	MODE_NONE = -1,
	MODE_GROUP,
	MODE_EAT
}
var mode = MODES.MODE_NONE
var cursorSlot = 0
var slot1 = null
var slot2 = null

func markSlot(idx: int):
	gridContainer.get_child(idx).modulate = Color.cyan

func unmarkSlot(idx: int):
	gridContainer.get_child(idx).modulate = Color.white

# Called when the node enters the scene tree for the first time.
func _ready():
	yield(LevelManager, "levelLoaded")
	#warning-ignore:return_value_discarded
	itemMenu.connect("id_pressed", self, "onMenuOptionSelected")
	#warning-ignore:return_value_discarded
	connect("visibility_changed", self, "on_visibility_changed")

func on_visibility_changed():
	if visible:
		grab_focus()

func update_menu_for_slot(slot: int):
	itemMenu.clear()
	itemMenu.add_item("INVENTORY_GROUP", MODES.MODE_GROUP)
	
	if inventoryToDraw.items[slot].empty():
		return
	
	var item: Dictionary = inventoryToDraw.expandItem(inventoryToDraw.items[slot])
	
	if item.has("on_eat"):
		itemMenu.add_item("INVENTORY_EAT", MODES.MODE_EAT)

func update_stats_for_slot(slot: int):
	statLabel.clear()
	
	if inventoryToDraw.items[slot].empty():
		return
	
	var item: Dictionary = inventoryToDraw.expandItem(inventoryToDraw.items[slot])
	if item.has("on_eat"):
		if item["on_eat"].has("hp"):
			
			statLabel.add_image(load("res://ui/heart2.png"))
			if item["on_eat"].has("hp_display"):
				statLabel.append_bbcode(item["on_eat"]["hp_display"])
			else:
				var av = Math.average(item["on_eat"]["hp"])
				statLabel.append_bbcode(str(stepify(av, 0.01)))

func fill():
	for child in gridContainer.get_children():
		gridContainer.remove_child(child)
	
	for item in inventoryToDraw.items:
		
		var newSlot = inventoryItemScene.instance()
		gridContainer.add_child(newSlot, true)
		
		if not item.empty():
			var expandedItem = inventoryToDraw.expandItem(item)
			newSlot.setItem(expandedItem.id)
			newSlot.setAmount(item.amount)
	
	showItemDescFor(cursorSlot)

func slotToPos(slot: int) -> Vector2:
	var width: int = gridContainer.columns
	
	#warning-ignore:integer_division
	var row := floor(slot/width)
	var column := slot-(row*width)
	return Vector2(column, row)

func posToSlot(pos: Vector2) -> int:
	var width = gridContainer.columns
	return pos.y * width + pos.x

func _gui_input(event):
	if event.is_action_pressed("ui_cancel"):
		if mode == MODES.MODE_GROUP:
			mode = MODES.MODE_NONE
			unmarkSlot(slot1)
		else:
			hide()
			return
	elif event.is_action_pressed("inventory"):
		hide()
		return
	if inventoryToDraw.taken_slots() > 0:
		if event.is_action_pressed("ui_accept"):
			match mode:
				# show menu
				MODES.MODE_NONE:
					accept_event()
					
					var inventoryItem = gridContainer.get_child(cursorSlot)
					var rect := Rect2(inventoryItem.rect_position, inventoryItem.rect_size)
					
					rect.position.y += inventoryItem.rect_size.y
					
					update_menu_for_slot(cursorSlot)
					itemMenu.popup(rect)
				
				# combine/swap items
				MODES.MODE_GROUP:
					accept_event()
					# deselect first item
					if cursorSlot == slot1:
						unmarkSlot(slot1)
						self.slot1 = null
						
						mode = MODES.MODE_NONE
					# combine/swap items
					else:
						self.slot2 = cursorSlot
						markSlot(slot2)
						
						var firstItem = inventoryToDraw.items[slot1]
						var secondItem = inventoryToDraw.items[slot2]
						if firstItem.empty() or secondItem.empty():
							# One of them empty, swapping
							inventoryToDraw.swap_slots(slot1, slot2)
						elif inventoryToDraw.expandItem(firstItem).id != inventoryToDraw.expandItem(secondItem).id:
							# Ids don't match, swapping
							inventoryToDraw.swap_slots(slot1, slot2)
						else:
							# Everything should match, combining
							inventoryToDraw.combine_slots(slot1, slot2)
						
						var bslot1 = slot1
						var bslot2 = slot2
						self.slot1 = null
						self.slot2 = null
						
						mode = MODES.MODE_NONE
						yield(get_tree().create_timer(0.5), "timeout")
						unmarkSlot(bslot1)
						unmarkSlot(bslot2)

		if event.is_action_pressed("ui_left") or event.is_action_pressed("ui_right") or event.is_action_pressed("ui_down") or event.is_action_pressed("ui_up"):
			accept_event()
			var pos = slotToPos(cursorSlot)
			
			var numItems := inventoryToDraw.items.size()
			var itemsPerRow: int = gridContainer.columns
			
			if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down"):
				pos.y += int(event.is_action_pressed("ui_down")) - int(event.is_action_pressed("ui_up"))
				pos.y = wrapi(pos.y, 0, num_items_in_column(pos.x, numItems, itemsPerRow))
			
			if event.is_action_pressed("ui_left") or event.is_action_pressed("ui_right"):
				pos.x += int(event.is_action_pressed("ui_right")) - int(event.is_action_pressed("ui_left"))
				pos.x = wrapi(pos.x, 0, num_items_in_row(pos.y, numItems, itemsPerRow))
			
			cursorSlot = posToSlot(pos)
			var slot: PanelContainer = gridContainer.get_child(cursorSlot)
			cursor.position = slot.rect_position + slot.rect_size/2
			
			showItemDescFor(cursorSlot)
			update_stats_for_slot(cursorSlot)
		

func onMenuOptionSelected(option: int):
	match option:
		MODES.MODE_GROUP:
			mode = MODES.MODE_GROUP
			slot1 = cursorSlot
			markSlot(slot1)
		MODES.MODE_EAT:
			var item: Dictionary = inventoryToDraw.expandItem(inventoryToDraw.items[cursorSlot])
			for effect in item["on_eat"]:
				if effect.ends_with("_display"):
					continue
				match effect:
					"hp":
						var hpPool: Array = item["on_eat"][effect]
						randomize()
						hpPool.shuffle()
						if hpPool[0] < 0:
							LevelManager.level.player.takeDamageSourceless(abs(hpPool[0]))
						else:
							LevelManager.level.player.heal(hpPool[0])
			inventoryToDraw.remove_item_at_slot(cursorSlot, 1)

func onItemChanged(itemIndex):
	var itemSlot = gridContainer.get_child(itemIndex)
	if inventoryToDraw.items[itemIndex].empty():
		itemSlot.clearItem()
		itemSlot.clearAmount()
	else:
		var itemName = inventoryToDraw.items[itemIndex]["type"]
		itemSlot.setItem(inventoryToDraw.expandItemName(itemName)["id"])
		itemSlot.setAmount(inventoryToDraw.items[itemIndex]["amount"])
		
	showItemDescFor(cursorSlot)
		
func showItemDescFor(slot: int):
	var selectedItem: Dictionary = inventoryToDraw.items[slot]
	
	if selectedItem.empty():
		descLabel.text = ""
	else:
		var itemId: int = inventoryToDraw.expandItem(selectedItem)["id"]
		descLabel.text = tr("ITEM_DESCRIPTION_%d" % itemId)


func num_items_in_column(col_index, num_items, items_per_row):
	var finalRowIdx = floor(num_items/items_per_row)
	var remainder = num_items % items_per_row
	
	# column is x value
	if col_index < remainder:
		return finalRowIdx + 1
	return finalRowIdx

func num_items_in_row(row_index, num_items, items_per_row):
	var finalRowIdx = floor(num_items/items_per_row)
	
	# column is x value
	if row_index < finalRowIdx:
		return items_per_row
	else:
		# amount of items in last row
		return num_items % items_per_row
