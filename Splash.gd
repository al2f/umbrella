extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	var fileObj = File.new()
	
	LevelManager.canvasLayer.heartContainer.hide()
	
	if Config.options["slot"] != null:
		LevelManager.canvasLayer.heartContainer.show()
		
		if Config.options["level"] != null:
			if fileObj.file_exists(Config.get_save_file_path(Config.options["slot"])):
				LevelManager.load_game(Config.options["slot"], Config.options["level"])
			else:
				LevelManager.new_game(Config.options["slot"], Config.options["level"])
		else:
			if fileObj.file_exists(Config.get_save_file_path(Config.options["slot"])):
				LevelManager.load_game(Config.options["slot"])
			else:
				LevelManager.new_game(Config.options["slot"])
		queue_free()
		
	else:
		#warning-ignore:return_value_discarded
		$AnimationPlayer.connect("animation_finished", self, "on_animation_finished")
		$AnimationPlayer.play("intro")

func _input(event):
	if event.is_action_pressed("ui_accept"):
		on_animation_finished("intro")
		get_tree().set_input_as_handled()
	elif event.is_action_pressed("ui_cancel"):
		on_animation_finished("intro")
		get_tree().set_input_as_handled()

func on_animation_finished(_anim_name: String):
	LevelManager.loadTiledLevel("titlescreen")
	get_node("/root").call_deferred("remove_child", self)
