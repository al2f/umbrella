extends "res://tiled/level_with_player.gd"

var fileObj := File.new()
var previewSaveSprite := Sprite.new()
var doors := {}
var textures := {}

var selected_slot := -1

const SAVE_NUMBER = 4

func _ready():
	player.inventory.clear()
	yield(LevelManager, "levelLoaded")
	player.position = get_object("main").position
	
	LevelManager.canvasLayer.inventoryDisplay.fill()
	LevelManager.canvasLayer.heartContainer.hide()
	LevelManager.canvasLayer.inventoryDisplay.hide()
	LevelManager.canvasLayer.force_hide_label()
	
	add_child(previewSaveSprite, true)
	move_child(previewSaveSprite, 1)
	
	previewSaveSprite.z_index = -1
	
	previewSaveSprite.region_enabled = true
	previewSaveSprite.region_rect = Rect2(24, 12, 280, 156)
	previewSaveSprite.centered = true
	
	previewSaveSprite.position = Vector2(272, 133)
	
	var delete_door = get_node("objects/deleteYes")
	delete_door.connect("entered_by_player", self, "deleteSave")
	
	if has_node("objects/version_label"):
		get_node("objects/version_label").text = Config.get_build_info()
	
	for i in range(SAVE_NUMBER):
		if has_node("objects/save%d" % i):
			var door: Area2D = get_node("objects/save%d" % i)
			
			var slot = int(door.name.lstrip("save"))
			doors[slot] = door
			
			if fileObj.file_exists(Config.get_save_file_path(slot)):
				if fileObj.file_exists(Config.get_save_screenshot_path(slot)):
					textures[slot] = Global.load_external_tex(Config.get_save_screenshot_path(slot))
				door.get_node("Label").text = tr("LOAD_GAME")
			else:
				textures[slot] = null
				door.get_node("Label").text = tr("NEW_GAME")
			
			setup_beam(door, slot)
			
			if fileObj.file_exists(Config.get_save_file_path(slot)):
				
				var saveData := Config.save_to_dict(slot)
				var maxHearts: int = saveData["general"]["player"]["max_health"]
				var hearts: int = saveData["general"]["player"]["health"]
				
				var lampType: int = saveData["game"].get("lampType", 0)
				door.set_type(lampType)
				
				print("Displaying file ", slot, ".cfg")
				print("* ", hearts, "/", maxHearts, " hearts\n")
				
				door.setHearts(hearts)
				door.setMaxHearts(maxHearts)
		
			#warning-ignore:return_value_discarded
			door.connect("entered_by_player", self, "loadSave")
			
		if has_node("objects/delete%d" % i):
			#FIXME: remove duplicated lines that are mostly copied from save door logic above
			
			var door: Area2D = get_node("objects/delete%d" % i)
			
			var slot = int(door.name.lstrip("save"))
			
			if fileObj.file_exists(Config.get_save_file_path(slot)):
				
				var saveData := Config.save_to_dict(slot)
				
				var lampType: int = saveData["game"].get("lampType", 0)
				door.set_type(lampType)
				
				setup_beam(door, slot)
				
				#warning-ignore:return_value_discarded
				door.connect("entered_by_player", self, "on_delete_pick_entered")
			else:
				door.visible = false
				door.get_node("CollisionShape2D").disabled = true
				door.get_node("Label").text = ""
	
func on_delete_pick_entered(sourceDoor, destLevel, _destDoor, proportions, focusCamera):
	_levelManager.move_player_to_door(sourceDoor, destLevel, "deleteNo", proportions, focusCamera)
	previewSaveSprite.position = get_node("objects/deletePreviewPos").position

func loadSave(sourceDoor, _destLevel, _destDoor, _proportions, _focusCamera):
	LevelManager.canvasLayer.heartContainer.show()
	
	var slot = int(sourceDoor.name.lstrip("save"))
	
	print("Loading save #", slot, " from titlescreen.")
	
	LevelManager.dropLevel()
	if not fileObj.file_exists(Config.get_save_file_path(slot)):
		LevelManager.new_game(slot)
	else:
		LevelManager.load_game(slot)

func deleteSave(_sourceDoor, _destLevel, _destDoor, _proportions, _focusCamera):
	
	var dir = Directory.new()
	
	if fileObj.file_exists(Config.get_save_file_path(selected_slot)):
		dir.remove(Config.get_save_file_path(selected_slot))
	if fileObj.file_exists(Config.get_save_screenshot_path(selected_slot)):
		dir.remove(Config.get_save_screenshot_path(selected_slot))
	
	var exisitingSaveDoor = get_node("objects/save%s" % selected_slot)
	
	exisitingSaveDoor.set_type(0)
	textures[selected_slot] = null
	
	# hide delete door
	get_node("objects/delete%d" % selected_slot).visible = false
	
	# clear preview texture
	previewSaveSprite.texture = null
	
	exisitingSaveDoor.get_node("Label").text = tr("NEW_GAME")
	exisitingSaveDoor.hideHearts()
	

func on_beam_entered(door: Area2D, slot: int):
	selected_slot = slot
	
	previewSaveSprite.position.x = door.position.x
	previewSaveSprite.position.y = door.position.y - door.height/2 - previewSaveSprite.region_rect.size.y/2
	if textures.has(slot):
		previewSaveSprite.texture = textures[slot]
	else:
		previewSaveSprite.texture = load("res://ui/missing-save-texture.png")

func setup_beam(door: Area2D, slot: int):
	var area := Area2D.new()
	var coll := CollisionShape2D.new()
	coll.shape = RectangleShape2D.new()
	area.set_script(load("res://objects/0triggers/titlescreen_beam.gd"))
	area.door = door
	area.slot = slot
	
	#warning-ignore:return_value_discarded
	area.connect("previewSave", self, "on_beam_entered")
	
	area.name = "preview%s" % door.name
	get_node("objects").add_child(area)
	area.add_child(coll, true)
	var shape: CollisionShape2D =  area.get_node("CollisionShape2D")
	area.position = door.position
	shape.shape.extents.x = door.width/1.5
	shape.shape.extents.y = 100
