<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.9.2" name="doors.tsx" tilewidth="64" tileheight="64" tilecount="6" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="door">
  <properties>
   <property name="keep" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="../../objects/door/frames/titlescreenDoor.png"/>
 </tile>
 <tile id="1" type="door">
  <properties>
   <property name="keep" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="../../objects/door/frames/woodenDoor.png"/>
 </tile>
 <tile id="2" type="door">
  <properties>
   <property name="keep" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="../../objects/door/frames/caveDoor.png"/>
 </tile>
 <tile id="3" type="door">
  <properties>
   <property name="keep" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="../../objects/door/frames/swampDoor.png"/>
 </tile>
 <tile id="4" type="door">
  <properties>
   <property name="keep" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="../../objects/door/frames/grassDoor.png"/>
 </tile>
 <tile id="5" type="door">
  <properties>
   <property name="keep" type="bool" value="true"/>
  </properties>
  <image width="64" height="64" source="../../objects/door/frames/saveGolem.png"/>
 </tile>
</tileset>
