<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.9.2" name="bushes" tilewidth="24" tileheight="32" tilecount="3" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="bush">
  <properties>
   <property name="itemName" value="fungus"/>
  </properties>
  <image width="24" height="32" source="../../items/000_editor.png"/>
 </tile>
 <tile id="1" type="bush">
  <properties>
   <property name="branches" type="int" value="0"/>
   <property name="itemName" value="moss"/>
  </properties>
  <image width="14" height="13" source="../../items/001.png"/>
 </tile>
 <tile id="4" type="bush">
  <properties>
   <property name="branches" type="int" value="0"/>
   <property name="itemName" value="blueberries"/>
  </properties>
  <image width="9" height="13" source="../../items/003.png"/>
 </tile>
</tileset>
