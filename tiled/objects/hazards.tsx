<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.9.2" name="hazards" tilewidth="16" tileheight="32" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0" type="stalactite">
  <image width="16" height="32" source="../../objects/stalactite/stalactite.png"/>
 </tile>
 <tile id="2" type="stalagmite">
  <image width="16" height="32" source="../../objects/stalagmite/stalagmite.png"/>
 </tile>
</tileset>
