<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="saves.tsx" tilewidth="16" tileheight="42" tilecount="5" columns="5">
 <image source="../../objects/lamp/lamp.png" trans="ffffff" width="80" height="42"/>
 <tile id="0" type="save"/>
 <tile id="1" type="save">
  <properties>
   <property name="lampType" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="2" type="save">
  <properties>
   <property name="lampType" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="3" type="save">
  <properties>
   <property name="lampType" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="4" type="save">
  <properties>
   <property name="lampType" type="int" value="4"/>
  </properties>
 </tile>
</tileset>
