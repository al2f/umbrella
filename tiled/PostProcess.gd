extends Node

func _post_process(level: Node2D):
	for layer in level.get_children():
		for o in layer.get_children():
			match layer.name:
				"level":
					pass
				"meta":
					if o.name == "background":
						var parallaxBackground := ParallaxBackground.new()
						parallaxBackground.name = "ParallaxBackground"
						level.add_child(parallaxBackground, true)
						
						var properties = o.get_meta_list()
						
#						for property in properties:
#							print(property)
						
#							var parts: Array = property.split("__")
#
#							if len(parts) == 2:
#								var layerNumber: int = int(parts[0])
#								var layerProperty: String = parts[1]
#
#								if parallaxBackground.get_node_or_null("layer%d" % layerNumber) == null:
#									var parallaxLayer := ParallaxLayer.new()
#									parallaxBackground.add_child(parallaxLayer)
#									parallaxLayer.name = "layer%d" % layerNumber
#
#								var parallaxLayer : ParallaxLayer = parallaxBackground.get_node("layer%d" % layerNumber)
#
#								var value = o.get_meta(property)
#								if value != null:
#									match layerProperty:
#										"scroll_x":
#											parallaxLayer.motion_scale.x = value
#										"scroll_y":
#											parallaxLayer.motion_scale.y = value
#										"texture":
#											var sprite := Sprite2D.new()
#											sprite.centered = false
#
#											var texture: Texture2D = load("res://art/%s" % value)
#											sprite.texture = texture
#											parallaxLayer.add_child(sprite, true)
#
#											parallaxLayer.motion_mirroring = texture.get_size()
#										"offset_x":
#											pass
#										"offset_y":
#											pass
				"triggers":
					if o.has_meta("class"):
						var type = o.get_meta("class")
					
						if o is StaticBody2D:
							var path = "res://objects/0triggers/" + type + "_trigger.gd"
							var triggerScript = load(path)
							var old_node = o
							var a := Area2D.new()
							o.replace_by(a)
							o.queue_free()
							a.set_script(triggerScript)
							for thing in a.get_children():
								if thing is CollisionShape2D:
									thing.name = "CollisionShape2D"
							print("Set up ", type, " trigger")
	
	var climbableTilemap = level.get_node_or_null("climbable")
	if climbableTilemap == null:
		printerr("Level %s is missing a 'climbable' tile layer!" % LevelManager.getLevelName())
		get_tree().quit(1)
	var climbableArea = load("res://objects/ladder/ladder.tscn").instantiate()
	level.add_child(climbableArea, true)

	level.remove_child(climbableTilemap)
	climbableArea.add_child(climbableTilemap)
#	climbableTilemap.collision_use_parent = true
	
	
			
	if level.has_node("meta"):
		level.get_node("meta").queue_free()
	
	return level

## For godot 4.
## Unfinished function to make a tilemap feed its collision to an area2D
#func tilemap_use_parent(tilemap: TileMap, area: Area2D):
#	var layer = 0
#	var cells = tilemap.get_used_cells(layer)
#	var collision_shape = CollisionPolygon2D.new()
#
#	for c in cells:
#		var data = tilemap.get_cell_tile_data(layer, c)
#		var points = data.get_collision_polygon_points(0, layer)
#
#		collision_shape.polygon = points
#		collision_shape.position = c*tilemap.cell_quadrant_size + Vector2i(tilemap.cell_quadrant_size, tilemap.cell_quadrant_size)/2
#		area.add_child(collision_shape)

#					if properties["name"] == "level":
#						#TODO: set up music
#						var c = 1- properties["darkness"]
#						var canvasModulate = CanvasModulate.new()
#						canvasModulate.name = "darkness"
#						level.add_child(canvasModulate)
#						canvasModulate.color = Color(c, c, c)
#
#						cameraBounds = Rect2(tiledObject.position, Vector2(properties["width"], properties["height"]))
#
#						node.remove_child(tiledObject)
#
#						if not properties.get("script") in [null, ""]:
#							print("setting level script to res://tiled/", properties["script"])
#							level.set_script(load("res://tiled/" + properties["script"]))
#						else:
#							level.set_script(load("res://level.gd"))
#					# Remove object that was on meta layer, as we don't want a walkable "background"
#					# object for instance.
#					tiledObject.queue_free()
