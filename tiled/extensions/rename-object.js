var action = tiled.registerAction("RenameObject", function(action) {
	tiled.log("thing")
	if(tiled.activeAsset.currentLayer.isObjectLayer){
		let objects = tiled.activeAsset.currentLayer.objects
		
		for (i=0;i<objects.length;i++){
			let obj = objects[i]
			if (obj.selected) {
				let ans = tiled.prompt("Enter object name", obj.name)
				if (ans != "") {
					obj.name = ans
				}
			}
		}
	}
})

tiled.extendMenu("MapView.Objects", [
    { action: "RenameObject"},
]);

action.text = "Rename Object"
action.shortcut = "F2"


