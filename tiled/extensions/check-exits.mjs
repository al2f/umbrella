import { object_exists_with_name, get_transitions, focusObject} from "level.mjs";

var project = tiled.project
if(project.fileName == ""){
	tiled.error("Project not open")
}
var project_path = tiled.project.folders[0]
							

tiled.assetSaved.connect(function(asset) {
	var transitions = get_transitions(asset)
	
	var outgoing_levels = {}
	
	transitions.forEach(function(obj){
		if (["transition", "transition_vertical", "door"].includes(obj.type)) {
			let level_name = obj.resolvedProperty("dest_level")
			let door_name = obj.resolvedProperty("dest_door")
			
			if([undefined, ""].includes(level_name)){
				tiled.error(obj.name + " does not have 'dest_level'", focusObject(obj))
			}
			if([undefined, ""].includes(door_name)){
				tiled.error(obj.name + " does not have 'dest_door'", focusObject(obj))
			}
			if(![undefined, ""].includes(level_name)){
				tiled.log("dest_door is " + door_name)
				
				var level_path = project_path + "/levels/" + level_name + ".tmx"
				if (File.exists(level_path)){
					var mapFmt = tiled.mapFormatForFile(level_path)
					var other_level = mapFmt.read(level_path)
					
					
					tiled.log("Looking for " + door_name + " in " + level_path)
					
					
					var other_transitions = get_transitions(other_level)
					var exists = object_exists_with_name(door_name, other_transitions)
					if(!exists){
						tiled.error("Transition '" + door_name + "' referenced in '" + obj.name + "' does not exist in level '" + level_name + "'", focusObject(obj))
					}
				} else {
					tiled.error("Level '" + level_path + "' referenced in '" + obj.name + "' does not exist", focusObject(obj))
				}
			}
		}
	})
})

