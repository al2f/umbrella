export { object_exists_with_name, get_transitions, focusObject, get_layer};

function object_exists_with_name(namea, objects){
	return objects.some(function(obj){
		return obj.name == namea
	})
}
function get_layer(asset, name){
	if (asset.isTileMap) {
		for(var i=0;i<asset.layers.length;i++) {
			let layer = asset.layers[i]
			if (layer.name == name) {
				return layer
			} 
		}
	}
}

function get_transitions(asset){
	var res = []
	var triggerLayer = get_layer(asset, "triggers")
	
	if (triggerLayer.isObjectLayer) {
		triggerLayer.objects.forEach(function(obj){
			if (["transition", "transition_vertical", "door"].includes(obj.type)) {
				res.push(obj)
			}
		})
	}
	return res
}
function focusObject(obj){
	tiled.mapEditor.currentMapView.centerOn(obj.x + obj.width/2, obj.y + obj.height/2)
}