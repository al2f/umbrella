extends "res://tiled/level_base.gd"

var player: KinematicBody2D
var post_player_add_function := ["", []]

func _ready():
	player = load("res://objects/player/player.tscn").instance()
	add_child(player)
	
	var method = post_player_add_function[0]
	var args = post_player_add_function[1]
	if method != "":
		callv(method, args)
	
	print("level-with-player-based  script loaded")

func disconnect_signals():
	.disconnect_signals()
#	var ladders := get_objects_by_type("ladder")
#
#	for l in ladders:
#		if l.is_connected("entered_by_player", player.on_ladder_entered):
#			l.disconnect("entered_by_player", player.on_ladder_entered)
	
	for rope in get_objects_by_type("rope"):
		rope.disconnect("entered_by_player", player, "on_roped")

func reconnect_signals():
	.reconnect_signals()
	
#	var ladders := get_objects_by_type("ladder")
#	for l in ladders:
#		l.connect("entered_by_player", player.on_ladder_entered.bind(l))
		#l.connect("exited_by_player", player.on_ladder_exited)
	
	player = get_node("player")
	if player == null:
		Logger.print_err(ERR_DOES_NOT_EXIST, "No player found in level!")
	
	for rope in get_objects_by_type("rope"):
		rope.connect("entered_by_player", player, "on_roped")

func _unhandled_input(event):
	
	if Config.debug:
		if event is InputEventKey and event.is_pressed():
			match event.scancode:
				KEY_W:
					get_tree().set_input_as_handled()
					player.camera.limit_left = -10000000
					player.camera.limit_right = 10000000
					player.camera.limit_top = -10000000
					player.camera.limit_bottom = 10000000
				KEY_D:
					get_tree().set_input_as_handled()
					player.noclip = !player.noclip
					player.set_collision_layer_bit(0, !player.noclip)
					player.set_collision_mask_bit(3, !player.noclip)
				KEY_Q:
					get_tree().set_input_as_handled()
					player.takeDamageSourceless(1)
				KEY_BACKSPACE:
					get_tree().set_input_as_handled()
					player.camera.zoom = Vector2.ONE
				KEY_BRACKETLEFT:
					get_tree().set_input_as_handled()
					player.camera.zoom += Vector2(0.1,0.1)
				KEY_BRACKETRIGHT:
					get_tree().set_input_as_handled()
					player.camera.zoom -= Vector2(0.1,0.1)

func move_player_to_door(doorName:String, player_was_facing_right: bool, player_was_right_of_door: bool, player_was_moving_upwards: bool, proportions: Vector2) -> void:
	
	var doors: Array = get_objects_by_type("door")
	var transitions: Array = get_triggers_by_type("transition")
	var transitionsVert: Array = get_triggers_by_type("transition_vertical")
	
	# loop through all doors until we find the needed one
	for door in doors:
		if door.name == doorName:
			player.position = door.position
			# remove player from the 'interact' layer to prevent the player from getting stuck
			# in an infinite loop teleporting between two doors
			player.set_collision_layer_bit(1, false)
			player.camera.current = true
			return
	
	for trans in transitions:
		if trans.name == doorName:
			var topLeft = trans.position - trans.size/2
			
			player.position = trans.get_proportionate_position(proportions)
			
			if player_was_right_of_door:
				player.position.x = topLeft.x - player.WIDTH
			else:
				player.position.x = topLeft.x + trans.width + player.WIDTH
			
			player.camera.current = true
			return
	
	for trans in transitionsVert:
		if trans.name == doorName:
			var topLeft = trans.position - trans.size/2
			
			var topLeftOnly = trans.leftTop and not trans.rightTop
			var topRightOnly = trans.rightTop and not trans.leftTop
			var topBoth = trans.leftTop and trans.rightTop
			
			var bottomLeftOnly = trans.leftBottom and not trans.rightBottom
			var bottomRightOnly = trans.rightBottom and not trans.leftBottom
			var bottomBoth = trans.leftBottom and trans.rightBottom
			
			var bottomAny = trans.leftBottom or trans.rightBottom
			var topAny = trans.leftTop or trans.rightTop
			
			if !player_was_moving_upwards:
				player.position.y = topLeft.y + trans.height + player.HEIGHT
				if not bottomAny:
					player.position.x = trans.get_proportionate_position(proportions).x
					player.camera.current = true
					return
				elif bottomLeftOnly or (bottomBoth and !player_was_facing_right):
					player.position.x = topLeft.x + 32
					player.puppet_fall_left()
					player.camera.current = true
					return
				elif bottomRightOnly or (bottomBoth and player_was_facing_right):
					player.position.x = topLeft.x + trans.width - 32
					player.puppet_fall_right()
					player.camera.current = true
					return
				
			else:
				player.position.y = topLeft.y - player.HEIGHT
				if not topAny:
					player.position.x = trans.get_proportionate_position(proportions).x
					player.camera.current = true
					return
				elif topLeftOnly or (topBoth and !player_was_facing_right):
					player.position.x = topLeft.x + 48
					player.puppet_jump_left()
					player.camera.current = true
					return
				elif topRightOnly or (topBoth and player_was_facing_right):
					player.position.x = topLeft.x + trans.width - 48
					player.puppet_jump_right()
					player.camera.current = true
					return
				
				
	Logger.print_err(ERR_DOES_NOT_EXIST, "No doors found with name '%s'." % doorName)
