<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.10.2" name="cave" tilewidth="16" tileheight="16" tilecount="450" columns="30">
 <image source="../../tilesets/cave.png" width="480" height="240"/>
 <tile id="13">
  <objectgroup draworder="index" id="2">
   <object id="1" x="14" y="16">
    <polygon points="0,0 2,-8 2,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="14">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index" id="2">
   <object id="1" x="5" y="16">
    <polygon points="0,0 9,-16 11,-16 11,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="16">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="20">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="21">
  <objectgroup draworder="index" id="3">
   <object id="3" x="0" y="0">
    <polygon points="0,0 16,0 16,12 12,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="43">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="16">
    <polygon points="3,0 10,-5 14,-16 16,-16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="44">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="45">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="10">
    <polygon points="0,0 0,6 16,6 16,-10 5,-10"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="49">
  <objectgroup draworder="index" id="2">
   <object id="1" x="12" y="16">
    <polygon points="0,0 4,-2 4,-16 -12,-16 -12,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="50">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,0 16,6 0,14"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="51">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 12,0 0,6"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="73">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="77">
  <objectgroup draworder="index" id="2">
   <object id="1" x="13" y="16">
    <polygon points="0,0 3,-2 3,-16 -13,-16 -13,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="78">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,14 16,6 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="79">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 12,0 0,6"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="103">
  <objectgroup draworder="index" id="2">
   <object id="1" x="16" y="4">
    <polygon points="0,0 -3,2 -12,2 -16,6 -16,12 0,12"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="104">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="3">
    <polygon points="0,1 6,-3 16,-3 16,13 0,13"/>
   </object>
  </objectgroup>
 </tile>
 <wangsets>
  <wangset name="Terrains" type="mixed" tile="121">
   <wangcolor name="cave" color="#ff0000" tile="31" probability="1"/>
   <wangcolor name="cave_bg" color="#00ff00" tile="362" probability="1"/>
   <wangcolor name="cave_fg" color="#0000ff" tile="159" probability="1"/>
   <wangtile tileid="9" wangid="1,1,1,0,1,1,1,1"/>
   <wangtile tileid="10" wangid="1,1,1,1,1,0,1,1"/>
   <wangtile tileid="31" wangid="0,0,1,1,1,0,0,0"/>
   <wangtile tileid="32" wangid="0,0,1,1,1,1,1,0"/>
   <wangtile tileid="33" wangid="0,0,0,0,1,1,1,0"/>
   <wangtile tileid="39" wangid="1,0,1,1,1,1,1,1"/>
   <wangtile tileid="40" wangid="1,1,1,1,1,1,1,0"/>
   <wangtile tileid="61" wangid="1,1,1,1,1,0,0,0"/>
   <wangtile tileid="62" wangid="1,1,1,1,1,1,1,1"/>
   <wangtile tileid="63" wangid="1,0,0,0,1,1,1,1"/>
   <wangtile tileid="91" wangid="1,1,1,0,0,0,0,0"/>
   <wangtile tileid="92" wangid="1,1,1,0,0,0,1,1"/>
   <wangtile tileid="93" wangid="1,0,0,0,0,0,1,1"/>
   <wangtile tileid="121" wangid="0,0,1,0,0,0,0,0"/>
   <wangtile tileid="122" wangid="0,0,1,0,0,0,1,0"/>
   <wangtile tileid="123" wangid="0,0,0,0,0,0,1,0"/>
   <wangtile tileid="159" wangid="3,3,0,0,0,3,3,3"/>
   <wangtile tileid="160" wangid="3,3,3,3,0,0,0,3"/>
   <wangtile tileid="181" wangid="0,0,0,3,0,0,0,0"/>
   <wangtile tileid="182" wangid="0,0,0,3,3,3,0,0"/>
   <wangtile tileid="183" wangid="0,0,0,0,0,3,0,0"/>
   <wangtile tileid="189" wangid="0,0,0,3,3,3,3,3"/>
   <wangtile tileid="190" wangid="0,3,3,3,3,3,0,0"/>
   <wangtile tileid="211" wangid="0,3,3,3,0,0,0,0"/>
   <wangtile tileid="212" wangid="3,3,3,3,3,3,3,3"/>
   <wangtile tileid="213" wangid="0,0,0,0,0,3,3,3"/>
   <wangtile tileid="241" wangid="0,3,0,0,0,0,0,0"/>
   <wangtile tileid="242" wangid="3,3,0,0,0,0,0,3"/>
   <wangtile tileid="243" wangid="0,0,0,0,0,0,0,3"/>
   <wangtile tileid="309" wangid="2,2,0,0,0,2,2,2"/>
   <wangtile tileid="310" wangid="2,2,2,2,0,0,0,2"/>
   <wangtile tileid="331" wangid="0,0,0,2,0,0,0,0"/>
   <wangtile tileid="332" wangid="0,0,0,2,2,2,0,0"/>
   <wangtile tileid="333" wangid="0,0,0,0,0,2,0,0"/>
   <wangtile tileid="339" wangid="0,0,0,2,2,2,2,2"/>
   <wangtile tileid="340" wangid="0,2,2,2,2,2,0,0"/>
   <wangtile tileid="361" wangid="0,2,2,2,0,0,0,0"/>
   <wangtile tileid="362" wangid="2,2,2,2,2,2,2,2"/>
   <wangtile tileid="363" wangid="0,0,0,0,0,2,2,2"/>
   <wangtile tileid="391" wangid="0,2,0,0,0,0,0,0"/>
   <wangtile tileid="392" wangid="2,2,0,0,0,0,0,2"/>
   <wangtile tileid="393" wangid="0,0,0,0,0,0,0,2"/>
  </wangset>
 </wangsets>
</tileset>
