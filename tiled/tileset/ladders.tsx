<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.10.2" name="ladders" tilewidth="16" tileheight="16" tilecount="9" columns="3">
 <image source="../../tilesets/ladders.png" width="48" height="48"/>
 <wangsets>
  <wangset name="Terrains" type="edge" tile="4">
   <wangcolor name="vine" color="#ff0000" tile="4" probability="1"/>
   <wangtile tileid="0" wangid="0,0,1,0,1,0,0,0"/>
   <wangtile tileid="1" wangid="0,0,0,0,1,0,0,0"/>
   <wangtile tileid="2" wangid="0,0,0,0,1,0,1,0"/>
   <wangtile tileid="3" wangid="0,0,1,0,0,0,0,0"/>
   <wangtile tileid="4" wangid="1,0,1,0,1,0,1,0"/>
   <wangtile tileid="5" wangid="0,0,0,0,0,0,1,0"/>
   <wangtile tileid="6" wangid="1,0,1,0,0,0,0,0"/>
   <wangtile tileid="7" wangid="1,0,0,0,0,0,0,0"/>
   <wangtile tileid="8" wangid="1,0,0,0,0,0,1,0"/>
  </wangset>
 </wangsets>
</tileset>
