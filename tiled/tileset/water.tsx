<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.10.2" name="water" tilewidth="16" tileheight="16" tilecount="28" columns="3">
 <image source="../../tilesets/waterfall.png" width="48" height="144"/>
 <tile id="0" probability="0.7">
  <animation>
   <frame tileid="0" duration="200"/>
   <frame tileid="1" duration="200"/>
   <frame tileid="2" duration="200"/>
  </animation>
 </tile>
 <tile id="3" probability="0.3">
  <animation>
   <frame tileid="3" duration="200"/>
   <frame tileid="4" duration="200"/>
   <frame tileid="5" duration="200"/>
  </animation>
 </tile>
 <tile id="4">
  <animation>
   <frame tileid="3" duration="200"/>
   <frame tileid="4" duration="200"/>
   <frame tileid="5" duration="200"/>
  </animation>
 </tile>
 <tile id="6" probability="0.7">
  <animation>
   <frame tileid="6" duration="200"/>
   <frame tileid="7" duration="200"/>
   <frame tileid="8" duration="200"/>
  </animation>
 </tile>
 <tile id="9" probability="0.3">
  <animation>
   <frame tileid="9" duration="200"/>
   <frame tileid="10" duration="200"/>
   <frame tileid="11" duration="200"/>
  </animation>
 </tile>
 <tile id="12" probability="0.7">
  <animation>
   <frame tileid="12" duration="200"/>
   <frame tileid="13" duration="200"/>
   <frame tileid="14" duration="200"/>
  </animation>
 </tile>
 <tile id="15" probability="0.3">
  <animation>
   <frame tileid="15" duration="200"/>
   <frame tileid="16" duration="200"/>
   <frame tileid="17" duration="200"/>
  </animation>
 </tile>
 <tile id="18" probability="0.7">
  <animation>
   <frame tileid="18" duration="200"/>
   <frame tileid="19" duration="200"/>
   <frame tileid="20" duration="200"/>
  </animation>
 </tile>
 <tile id="21" probability="0.3">
  <animation>
   <frame tileid="21" duration="200"/>
   <frame tileid="22" duration="200"/>
   <frame tileid="23" duration="200"/>
  </animation>
 </tile>
 <tile id="24" probability="0.7">
  <animation>
   <frame tileid="24" duration="200"/>
   <frame tileid="25" duration="200"/>
   <frame tileid="26" duration="200"/>
  </animation>
 </tile>
 <tile id="27" probability="0.3"/>
 <wangsets>
  <wangset name="Terrains" type="corner" tile="12">
   <wangcolor name="water" color="#ff0000" tile="0" probability="1"/>
   <wangtile tileid="0" wangid="0,1,0,1,0,1,0,1"/>
   <wangtile tileid="3" wangid="0,1,0,1,0,0,0,0"/>
   <wangtile tileid="4" wangid="0,0,0,0,0,1,0,1"/>
   <wangtile tileid="6" wangid="0,1,0,1,0,0,0,0"/>
   <wangtile tileid="9" wangid="0,0,0,0,0,1,0,1"/>
   <wangtile tileid="12" wangid="0,1,0,0,0,0,0,1"/>
  </wangset>
 </wangsets>
</tileset>
