<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.10.2" name="grass" tilewidth="16" tileheight="16" tilecount="110" columns="22">
 <image source="../../tilesets/grass.png" width="352" height="80"/>
 <wangsets>
  <wangset name="Terrains" type="mixed" tile="23">
   <wangcolor name="grass" color="#ff0000" tile="23" probability="1"/>
   <wangtile tileid="9" wangid="1,1,0,0,0,1,1,1"/>
   <wangtile tileid="10" wangid="1,1,1,1,0,0,0,1"/>
   <wangtile tileid="22" wangid="0,0,0,0,1,0,0,0"/>
   <wangtile tileid="23" wangid="0,0,1,1,1,0,0,0"/>
   <wangtile tileid="24" wangid="0,0,1,1,1,1,1,0"/>
   <wangtile tileid="25" wangid="0,0,0,0,1,1,1,0"/>
   <wangtile tileid="31" wangid="0,0,0,1,1,1,1,1"/>
   <wangtile tileid="32" wangid="0,1,1,1,1,1,0,0"/>
   <wangtile tileid="44" wangid="1,0,0,0,1,0,0,0"/>
   <wangtile tileid="45" wangid="1,1,1,1,1,0,0,0"/>
   <wangtile tileid="46" wangid="1,1,1,1,1,1,1,1"/>
   <wangtile tileid="47" wangid="1,0,0,0,1,1,1,1"/>
   <wangtile tileid="66" wangid="1,0,0,0,0,0,0,0"/>
   <wangtile tileid="67" wangid="1,1,1,0,0,0,0,0"/>
   <wangtile tileid="68" wangid="1,1,1,0,0,0,1,1"/>
   <wangtile tileid="69" wangid="1,0,0,0,0,0,1,1"/>
   <wangtile tileid="89" wangid="0,0,1,0,0,0,0,0"/>
   <wangtile tileid="90" wangid="0,0,1,0,0,0,1,0"/>
   <wangtile tileid="91" wangid="0,0,0,0,0,0,1,0"/>
  </wangset>
 </wangsets>
</tileset>
