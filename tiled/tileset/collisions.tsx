<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="collisions" tilewidth="16" tileheight="16" tilecount="2" columns="2">
 <image source="../../tilesets/collisions.png" width="32" height="16"/>
 <tile id="0">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
