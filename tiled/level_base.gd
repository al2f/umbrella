extends Node2D

var _levelManager: Node2D

func setLevelManager(thing):
	_levelManager = thing


func disconnect_signals():
	pass

func reconnect_signals():
	pass

# Get all nodes whose metadata `type` value matches type.
func get_nodes_by_type(type: String, node: Node) -> Array:
	var result = []
	for obj in node.get_children():
		if "class" in obj.get_meta_list():
			if obj.get_meta("class") == type:
				result.append(obj)
		elif "type" in obj.get_meta_list():
			if obj.get_meta("type") == type:
				result.append(obj)
	return result

func has_objects():
	return has_node("objects")

# Alias for getNodesByType() with path of "/root/level/objects".
func get_objects_by_type(type: String) -> Array:
	if has_node("objects"):
		return get_nodes_by_type(type, get_node("objects"))
	return []

# Alias for getNodesByType() with path of "/root/level/triggers".
func get_triggers_by_type(type: String) -> Array:
	return get_nodes_by_type(type, get_node("triggers"))

func get_object(name: String):
	return get_node_or_null("objects".plus_file(name))

func get_trigger(name: String):
	return get_node_or_null("triggers".plus_file(name))
	
func get_object_by_id(id: int):
	for obj in get_node("objects").get_children():
		if obj.has_meta("id"):
			if obj.get_meta("id") == id:
				return obj

func _unhandled_input(event):
	if event.is_action_pressed("debug"):
		Config.debug = !Config.debug
		get_tree().set_input_as_handled()
		return
	
	if event.is_action_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
		get_tree().set_input_as_handled()
		return
		
	if event.is_action_pressed("screenshot"):
		var i := 0
		var fileObj := File.new()
		var path := Global.get_screenshot_path(0)
		while fileObj.file_exists(ProjectSettings.globalize_path(path)):
			i += 1
			path = Global.get_screenshot_path(i)
		Global.captureScreen(path)
		LevelManager.canvasLayer.messageContainer.add_message(tr("MESSAGE_SCREENSHOT") % path)
		return
	
	
	if Config.debug:
		if event is InputEventKey and event.is_pressed():
			match event.scancode:
				
				KEY_E:
					get_tree().set_input_as_handled()
					_levelManager.dropAndLoadLevel(_levelManager.getLevelName())
				
				KEY_EQUAL, KEY_PLUS:
					get_tree().set_input_as_handled()
					Engine.time_scale += 0.1
					print("Game speed: ", Engine.time_scale)
				KEY_MINUS:
					get_tree().set_input_as_handled()
					Engine.time_scale -= 0.1
					print("Game speed: ", Engine.time_scale)
				KEY_0:
					get_tree().set_input_as_handled()
					Engine.time_scale = 1

var floatingLabelScene := load("res://ui/floatingLabel.tscn")


func label_item(glob_pos: Vector2, data: Dictionary):
	add_label(glob_pos, data, 2)
func label_health_change(glob_pos: Vector2, data):
	add_label(glob_pos, data, 0)
func label_health_change_npc(glob_pos: Vector2, data):
	add_label(glob_pos, data, 1)

func add_label(glob_pos: Vector2, data, type):
	var newLabel = floatingLabelScene.instance()
	newLabel.data = data
	newLabel.type = type
	newLabel.rect_global_position = glob_pos
	
	add_child(newLabel)

func reparent_particles(node: Particles2D):
	node.get_parent().remove_child(node)
	add_child(node)
	node.emitting = true
	yield(get_tree().create_timer(node.lifetime), "timeout")
	node.queue_free()
