extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var vec = Vector2(0,0)
var size = 24
# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.position = $Position2D.position

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed():
			print("mouse ", event.button_index)
	elif event is InputEventKey:
		if event.is_pressed() and !event.is_echo():
			print("keybd ", event.scancode)
	elif event is InputEventJoypadButton:
		if event.is_pressed():
			print("jybtn ", event.button_index)
			
		if event.is_pressed():
			get_node(str("btn", event.button_index)).color = Color(0,0,0)
		else:
			get_node(str("btn", event.button_index)).color = Color(1,1,1)
		
	elif event is InputEventJoypadMotion:
		if event.is_pressed():
			print("jyaxs ", event.axis, " ", event.axis_value)
			
		if event.axis == 0:
			print(event.axis_value, " horizontal")
			vec.x = event.axis_value
		if event.axis == 1:
			print(event.axis_value, " vertical")
			vec.y = event.axis_value
		
		var point = get_node("Position2D").position
		
		vec = vec.snapped(Vector2(0.01, 0.01))
		$Sprite.position = $Position2D.position + (vec.normalized()*size)

func _draw():
	draw_circle($Position2D.position, size + $Sprite.texture.get_size().x, Color(1,1,1,0.5))    
