extends Popup

const BASE_ICON_PATH:="res://ui/settings"
var eventIcons := {
	"InputEventKey":			"key.png",
	"InputEventMouseButton":	"mouse.png",
	"InputEventJoypadButton":	"joypad.png",
	"InputEventJoypadMotion":	"joystick.png",
}

var addIcon := load(BASE_ICON_PATH.plus_file("add.png"))
var replaceIcon := load(BASE_ICON_PATH.plus_file("replace.png"))
var removeIcon := load(BASE_ICON_PATH.plus_file("remove.png"))

var mouseButtons = [
	"INPUT_MOUSE_LEFT",
	"INPUT_MOUSE_RIGHT",
	"INPUT_MOUSE_MIDDLE",
	"INPUT_WHEEL_UP",
	"INPUT_WHEEL_DOWN",
	"INPUT_WHEEL_LEFT",
	"INPUT_WHEEL_RIGHT",
	"INPUT_MOUSE_EXTRA_1",
	"INPUT_MOUSE_EXTRA_2",
]

var actionDrawOrder := [
	"ui_left",
	"ui_right",
	"ui_up",
	"ui_down",
	
	"ui_accept",
	"ui_cancel",
	"interact",
	
	"jump",
	"umbrella",
	"inventory",
	"quit",
	
	"settings",
	"screenshot",
	"fullscreen",
	
	"ui_focus_next",
	"ui_focus_prev",
	
	"ui_home",
	"ui_end",
	
	"ui_select",
	
	"ui_page_up",
	"ui_page_down",
	
	"debug",
]

var userFriendlyNames := {
	"ui_accept": "INPUT_UI_ACCEPT",
	"ui_cancel": "INPUT_UI_CANCEL"
}

onready var scrollContainer = get_node("ScrollContainer/VBoxContainer")
onready var tree: Tree = scrollContainer.get_node("Tree")
onready var pressKeyPrompt := get_node("PopupDialog")
onready var pressKeyPromptLabel := pressKeyPrompt.get_node("Label")

func grab_focus():
	scrollContainer.get_node("Button").grab_focus()

func save_settings():
	ControlsManager.save_controls()

func save_and_back():
	save_settings()
	hide()
	
func _ready():
	tree.set_process_input(false)
	tree.set_process_unhandled_input(false)
	
	#warning-ignore:return_value_discarded
	tree.connect("button_pressed", self, "_on_tree_button_pressed")
	
	# connect back button to emit signal to return back to the menu
	scrollContainer.get_node("Button").connect("pressed", self, "save_and_back")
	
	var root = tree.create_item()
	tree.set_hide_root(true)
	
	for action in actionDrawOrder:
		if not InputMap.has_action(action):
			continue
		var actionLabel: TreeItem = tree.create_item(root)
		
		if action in userFriendlyNames:
			actionLabel.set_text(0, tr(userFriendlyNames[action]))
		else:
			actionLabel.set_text(0, action)
		actionLabel.set_meta("action", action)
		
		actionLabel.set_tooltip(0, action)
		actionLabel.add_button(0, addIcon, -1, false, tr("INPUT_ADD_EVENT"))
		
		for event in InputMap.get_action_list(action):
			addEventItem(actionLabel, event)

func addEventItem(parentItem: TreeItem, event: InputEvent):
	var eventDisplay: TreeItem = tree.create_item(parentItem)
	eventDisplay.set_text(0, eventToText(event))
#	print("Added ", eventToText(event), " as a child of ", parentItem.get_text(0))
	eventDisplay.add_button(0, replaceIcon, -1, false, tr("INPUT_REPLACE_EVENT"))
	eventDisplay.add_button(0, removeIcon, -1, false, tr("INPUT_REMOVE_EVENT"))
	eventDisplay.set_meta("event", event)
	setEventIcon(eventDisplay, event)

func setEventIcon(eventItem: TreeItem, event: InputEvent):
	if event.get_class() in eventIcons:
		eventItem.set_icon(0, load(BASE_ICON_PATH.plus_file(eventIcons[event.get_class()])))

func eventToText(event: InputEvent):
	match event.get_class():
		"InputEventKey":
			return OS.get_scancode_string(event.get_scancode_with_modifiers())
			
			#ctrl alt shift meta control
		"InputEventMouseButton":
			if event.button_index > mouseButtons.size():
				return tr("INPUT_MOUSE_BUTTON") % event.button_index
			return tr(mouseButtons[event.button_index-1])
			
		"InputEventJoypadButton":
			return tr("INPUT_JOYPAD_BUTTON") % event.button_index
		"InputEventJoypadMotion":
			if event.axis_value < 0:
				return tr("INPUT_JOYSTICK_AXIS_NEGATIVE") % event.axis
			elif event.axis_value > 0:
				return tr("INPUT_JOYSTICK_AXIS_POSITIVE") % event.axis
	return event.as_text()

func _on_tree_button_pressed(treeItem: TreeItem, column: int, id: int):
	
	# add event
	if treeItem.get_parent() == tree.get_root() and tree.get_root() != null:
		addEvent(treeItem)
	# Replace or remove event
	else:
		var actionName = treeItem.get_parent().get_meta("action")
		
		if InputMap.has_action(actionName):
			if treeItem.get_button_count(0) == 2:
				if id == 0:
					replaceEvent(actionName, treeItem)
				elif id == 1:
					removeEvent(actionName, treeItem)

func addEvent(actionItem: TreeItem):
	var actionName: String = actionItem.get_meta("action")
	var event = yield(getEvent(), "completed")
		
	InputMap.action_add_event(actionName, event)
	
	pressKeyPromptLabel.text = tr("INPUT_PRESS_KEY_ADD") % actionName
	addEventItem(actionItem, event)

func removeEvent(actionName: String, eventItem: TreeItem):
	InputMap.action_erase_event(actionName, eventItem.get_meta("event"))
	eventItem.free()

func replaceEvent(actionName:String, eventItem: TreeItem):
	pressKeyPromptLabel.text = tr("INPUT_PRESS_KEY_REPLACE") % eventItem.get_text(0)
	var event = yield(getEvent(), "completed")
	
	InputMap.action_erase_event(actionName, eventItem.get_meta("event"))
	InputMap.action_add_event(actionName, event)
	
	eventItem.set_meta("event", event)
	eventItem.set_text(0, eventToText(event))
	setEventIcon(eventItem, event)

func getEvent():
	pressKeyPrompt.show()
	var event = yield(pressKeyPrompt, "event_recieved")
	pressKeyPrompt.hide()
	
	return event

func getInputData():
	var result = {}
	if tree.get_root() != null:
		var currentActionItem: TreeItem = tree.get_root().get_children()
		while currentActionItem != null:
			var actionName = currentActionItem.get_text(0)
			if InputMap.has_action(actionName):
				result[actionName] = []
				var currentActionEventItem: TreeItem = currentActionItem.get_children()
				while currentActionEventItem != null:
					result[actionName].append(currentActionEventItem.get_meta("event"))
					currentActionEventItem = currentActionEventItem.get_next()
			currentActionItem = currentActionItem.get_next()
	return result
