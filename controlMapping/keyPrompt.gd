extends PopupDialog

signal event_recieved

func _ready():
	#warning-ignore:return_value_discarded
	connect("visibility_changed", self, "on_visibility_changed")

func on_visibility_changed():
	if visible:
		grab_focus()

func _gui_input(event: InputEvent) -> void:
	if event.get_class() in ["InputEventKey", "InputEventMouseButton", "InputEventJoypadButton", "InputEventJoypadMotion"]:
		if event is InputEventJoypadMotion:
			if event.is_pressed():
				emit_signal("event_recieved", event)
				accept_event()
		elif not event.is_pressed():
			emit_signal("event_recieved", event)
			accept_event()
