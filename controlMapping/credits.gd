extends PopupPanel

onready var backButton := get_node("ScrollContainer/VBoxContainer/Button")
onready var label := get_node("ScrollContainer/VBoxContainer/TextEdit")

func _ready():
	connect("visibility_changed", self, "on_visibility_changed")
	backButton.connect("pressed", self, "hide")
	
	var f := File.new()
	var err = f.open("res://CREDITS.txt", File.READ)
	if err == OK:
		var content = f.get_as_text()
		label.text = content
		

func on_visibility_changed():
	if visible:
		backButton.grab_focus()
