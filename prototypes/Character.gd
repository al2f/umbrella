extends KinematicBody2D

# This represents the player's inertia.
export (int, 0, 200) var inertia = 100

func _physics_process(_delta):

	# after calling move_and_slide()
	for index in get_slide_count():
		var collision = get_slide_collision(index)
		if collision.collider is RigidBody2D:
			collision.collider.apply_central_impulse(-collision.normal * inertia)
